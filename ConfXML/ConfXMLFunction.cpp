/* 
 * File:   ConfXMLFunction.cpp
 * Author: Kamil
 * 
 * Created on 11 maj 2012, 10:56
 */

#include "ConfXMLFunction.h"
#include "ConfXMLParamter.h"

IMPLEMENT_FACTORY(ConfXMLFunction, function)

void ConfXMLFunction::MapXMLTagsToMembers() {
    MapAttribute(&name, "name");

    MapMember(&parameterList, ConfXMLParameter::GetStaticTag());

    MapMember(&u, "u");
    MapMember(&v, "v");
}