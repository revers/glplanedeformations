/* 
 * File:   ConfXMLFunction.h
 * Author: Kamil
 *
 * Created on 11 maj 2012, 10:56
 */

#ifndef CONFXMLFUNCTION_H
#define	CONFXMLFUNCTION_H

#include "xmlObject.h"
#include "GString.h"

class ConfXMLFunction : public XMLObject {
    GString name;
    GString u;
    GString v;

    GList parameterList;
public:
    virtual void MapXMLTagsToMembers();

    DECLARE_FACTORY(ConfXMLFunction, function)

    ConfXMLFunction() {
    }

    const char* getName() const {
        return name.StrVal();
    }

    void setName(const char* name) {
        this->name = name;
    }

    GList* getParameterList() {
        return &parameterList;
    }

    const char* getU() const {
        return u.StrVal();
    }

    void setU(const char* u) {
        this->u = u;
    }

    const char* getV() const {
        return v.StrVal();
    }

    void setV(const char* v) {
        this->v = v;
    }
};


#endif	/* CONFXMLFUNCTION_H */

