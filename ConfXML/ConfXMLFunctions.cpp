/* 
 * File:   ConfXMLFunctions.cpp
 * Author: Kamil
 * 
 * Created on 11 maj 2012, 10:57
 */

#include "ConfXMLFunctions.h"
#include "ConfXMLFunction.h"

IMPLEMENT_FACTORY(ConfXMLFunctions, functions)

void ConfXMLFunctions::MapXMLTagsToMembers() {
    MapMember(&functionList, ConfXMLFunction::GetStaticTag());
}
