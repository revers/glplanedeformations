/* 
 * File:   ConfXMLFunctions.h
 * Author: Kamil
 *
 * Created on 11 maj 2012, 10:57
 */

#ifndef CONFXMLFUNCTIONS_H
#define	CONFXMLFUNCTIONS_H

#include "xmlObject.h"
#include "GString.h"

#include <boost/shared_ptr.hpp>

class ConfXMLFunctions : public XMLObject {
    GList functionList;
public:
    virtual void MapXMLTagsToMembers();

    DECLARE_FACTORY(ConfXMLFunctions, functions)

    ConfXMLFunctions() {
    }

    GList* getFunctionList() {
        return &functionList;
    }
};

typedef boost::shared_ptr<ConfXMLFunctions> ConfXMLPtr;

#endif	/* CONFXMLFUNCTIONS_H */

