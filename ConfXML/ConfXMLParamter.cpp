/* 
 * File:   ConfXMLParamter.cpp
 * Author: Kamil
 * 
 * Created on 11 maj 2012, 10:13
 */

#include "ConfXMLParamter.h"

IMPLEMENT_FACTORY(ConfXMLParameter, parameter)

void ConfXMLParameter::MapXMLTagsToMembers() {
    MapAttribute(&name, "name");
    MapAttribute(&shortName, "shortName");
    MapAttribute(&value, "value");
    MapAttribute(&step, "step");
    MapAttribute(&maxValue, "maxValue");
    MapAttribute(&minValue, "minValue");
}