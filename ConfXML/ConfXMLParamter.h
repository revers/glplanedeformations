/* 
 * File:   ConfXMLParamter.h
 * Author: Kamil
 *
 * Created on 11 maj 2012, 10:13
 */

#ifndef CONFXMLPARAMTER_H
#define	CONFXMLPARAMTER_H

#include "xmlObject.h"
#include "GString.h"

class ConfXMLParameter : public XMLObject {

    GString name;
    GString shortName;
    float value;
    float minValue;
    float maxValue;
    float step;
public:
    virtual void MapXMLTagsToMembers();

    DECLARE_FACTORY(ConfXMLParameter, parameter)

    ConfXMLParameter() {
    }

    float getMaxValue() const {
        return maxValue;
    }

    void setMaxValue(float maxValue) {
        this->maxValue = maxValue;
    }

    float getMinValue() const {
        return minValue;
    }

    void setMinValue(float minValue) {
        this->minValue = minValue;
    }

    float getValue() const {
        return value;
    }

    void setValue(float value) {
        this->value = value;
    }
    
    const char* getName() const {
        return name.StrVal();
    }

    void setName(const char* name) {
        this->name = name;
    }
    
    const char* getShortName() const {
        return shortName.StrVal();
    }

    void setShortName(const char* shortName) {
        this->shortName = shortName;
    }
    
    float getStep() const {
        return step;
    }

    void setStep(float step) {
        this->step = step;
    }
};

#endif	/* CONFXMLPARAMTER_H */

