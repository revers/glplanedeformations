/* 
 * File:   ControlPanel.cpp
 * Author: Revers
 * 
 * Created on 26 kwiecień 2012, 20:33
 */
#include <GL/glew.h>

#include <cmath>
#include <iostream>
#include <sstream>

#include "ControlPanel.h"

#include <boost/algorithm/string.hpp>

using namespace boost::filesystem;
using namespace std;

#define SETTINGS_TOOLBAR_WIDTH "300"
#define SETTINGS_TOOLBAR_HEIGHT "520"

const char* ControlPanel::COMPILATION_OK = "OK";
const char* ControlPanel::COMPILATION_FAILED = "ERROR";

bool ControlPanel::init(RendererPtr renderer, ConfXMLPtr xmlFunctions) {
    this->renderer = renderer;
    this->xmlFunctions = xmlFunctions;

    if (!TwInit(TW_OPENGL_CORE, NULL)) {
        cout << TwGetLastError() << endl;
        return false;
    }

    settingsBar = TwNewBar("Settings");

    TwDefine(" TW_HELP visible=false ");
    TwDefine(" Settings color='20 20 20' size='" SETTINGS_TOOLBAR_WIDTH " " SETTINGS_TOOLBAR_HEIGHT "'  ");
    TwDefine(" Settings valueswidth=165 fontSize=3 ");
    
    struct { int x; int y; } pos = { 10, 5 };
    TwSetParam(settingsBar, NULL, "position", TW_PARAM_INT32, 2, &pos);

    TwAddVarRW(settingsBar, "__Pause", TW_TYPE_BOOLCPP, &(paused), "label='Pause' group='Control'");

    if (!loadBackgroundImages()) {
        cout << "ControlPanel::loadBackgrounImages() FAILED!!" << endl;
        return false;
    }

    TwAddVarRW(settingsBar, "__Speed", TW_TYPE_FLOAT, &speed, " label='Speed' min=0.1 max=100.0 step=0.1 group='Control' ");

    TwAddVarCB(settingsBar, "__Level", TW_TYPE_INT32, setAALevelCallback, getAALevelCallback, this,
            "label='Level' min=1 max=10 step=1 group='Antialiasing' ");

    TwAddVarCB(settingsBar, "__Scale", TW_TYPE_FLOAT, setAAScaleCallback, getAAScaleCallback, this,
            "label='Scale' min=0.001 max=25.0 step=0.001 group='Antialiasing' ");



    if (!loadDynamicShaders()) {
        cout << "ControlPanel::loadDynamicShaders() FAILED!" << endl;
        return false;
    }
    
    TwAddVarCB(settingsBar, "__XMIN", TW_TYPE_FLOAT, setXMinCallback, getXMinCallback, this,
            "label='X Min' min=-100000.0 max=100000.0 step=0.01 group='Boundaries' ");
    TwAddVarCB(settingsBar, "__XMAX", TW_TYPE_FLOAT, setXMaxCallback, getXMaxCallback, this,
            "label='X Max' min=-100000.0 max=100000.0 step=0.01 group='Boundaries' ");
    TwAddVarCB(settingsBar, "__YMIN", TW_TYPE_FLOAT, setYMinCallback, getYMinCallback, this,
            "label='Y Min' min=-100000.0 max=100000.0 step=0.01 group='Boundaries' ");
    TwAddVarCB(settingsBar, "__YMAX", TW_TYPE_FLOAT, setYMaxCallback, getYMaxCallback, this,
            "label='Y Max' min=-100000.0 max=100000.0 step=0.01 group='Boundaries' ");

    TwCopyCDStringToClientFunc(copyCDStringToClient);
    TwAddVarRW(settingsBar, "__EquationU", TW_TYPE_CDSTRING, &strU,
            " label='Equation U' group='Functions' ");

    TwCopyCDStringToClientFunc(copyCDStringToClient);
    TwAddVarRW(settingsBar, "__EquationV", TW_TYPE_CDSTRING, &strV,
            " label='Equation V' group='Functions' ");

    addParameters();

    DynamicShaderPtr shader = dynamicShaders[0];
    renderer->setProgram(shader->getProgram());
    shader->refreshParameters();

    TwAddVarRO(settingsBar, "__STATUS", TW_TYPE_CDSTRING, &statusText,
            " label='Compilation' group='Info' ");
    
    TwAddVarRO(settingsBar, "__TIME", TW_TYPE_FLOAT, &totalTime, 
            " label='Time' group='Info' ");

    TwAddButton(settingsBar, "__ResetTime", resetTimeCallback,
            this, "label='Reset Time' group='Actions' ");
    TwAddButton(settingsBar, "__ResetBoundaries", resetBoundariesCallback,
            this, "label='Reset Boundaries' group='Actions' ");
    TwAddButton(settingsBar, "__ResetFuncParams", resetFuncParamsButtonCallback,
            this, "label='Reset Function Params' group='Actions' ");
    TwAddButton(settingsBar, "__Compile", compileButtonCallback,
            this, "label='Compile/Run (Enter)' group='Actions' ");

    return true;
}

bool ControlPanel::loadDynamicShaders() {

    GList* functionList = xmlFunctions->getFunctionList();
    GListIterator iter(functionList);

    cout << "-------------------------------------------------------------"
            << endl;

    while (iter()) {
        ConfXMLFunction* xmlFunction = static_cast<ConfXMLFunction*> (iter++);

        DynamicShaderPtr dynamicShader(new DynamicShader(xmlFunction));
        if (dynamicShader->init()) {
            dynamicShaders.push_back(dynamicShader);
        }
        cout << "-------------------------------------------------------------"
                << endl;
    }

    if (dynamicShaders.empty()) {
        return false;
    }

    size_t size = dynamicShaders.size();

    TwEnumVal* functionsEnum = new TwEnumVal[size];
    int i = 0;

    for (DynamicShadersVector::iterator iter = dynamicShaders.begin();
            iter != dynamicShaders.end();) {
        DynamicShaderPtr p = *(iter++);

        functionsEnum[i].Value = i;
        functionsEnum[i].Label = p->getName();
        i++;
    }

    TwType functionsType = TwDefineEnum("FunctionType", functionsEnum, size);
    TwAddVarCB(settingsBar, "__Function", functionsType, setFunctionListCallback,
            getFunctionListCallback, this, "label='Name' group='Functions' ");

    delete[] functionsEnum;

    return true;
}

void ControlPanel::addParameters() {
    if (parametersAdded) {
        cout << "ERROR: You cannot use ControlPanel::addParameters()"
                << " when parametersAdded = true!!" << endl;
        return;
    }

    DynamicShaderPtr shader = dynamicShaders[selectedShaderIndex];

    copyCDStringToClient(&strU, shader->getEquationU());
    copyCDStringToClient(&strV, shader->getEquationV());

    DynamicShaderParameters& parameters = shader->getParameters();

    for (int i = 0; i < parameters.size(); i++) {

        DynamicShaderParam& param = parameters[i];
        ConfXMLParameter* xmlParam = param.xmlParameter;
        const char* name = xmlParam->getName();

        std::ostringstream oss;
        oss << "label='" << name << "' ";
        oss << " min='" << xmlParam->getMinValue() << "' ";
        oss << " max='" << xmlParam->getMaxValue() << "' ";
        oss << " step='" << xmlParam->getStep() << "' ";
        oss << " group='Functions' ";

        const char* antParams = oss.str().c_str();
        cout << "ANT PARAMS = " << antParams << endl;

        ControlParamPair* cpp = new ControlParamPair(this, &param);
        ControlParamPairPtr pair(cpp);
        controlParamPairVector.push_back(pair);

        TwAddVarCB(settingsBar, name, TW_TYPE_FLOAT,
                setParameterCallback, getParameterCallback,
                cpp, antParams);
    }


    parametersAdded = true;
}

void ControlPanel::removeParameters() {
    if (!parametersAdded) {
        cout << "ERROR: You cannot use ControlPanel::removeParameters()"
                << " when parametersAdded = false!!" << endl;
        return;
    }

    DynamicShaderPtr shader = dynamicShaders[selectedShaderIndex];
    DynamicShaderParameters& parameters = shader->getParameters();

    for (int i = 0; i < parameters.size(); i++) {

        DynamicShaderParam& param = parameters[i];
        ConfXMLParameter* xmlParam = param.xmlParameter;
        const char* name = xmlParam->getName();

        TwRemoveVar(settingsBar, name);
    }

    controlParamPairVector.clear();

    parametersAdded = false;
}

void ControlPanel::resetFunction() {
    DynamicShaderPtr shader = dynamicShaders[selectedShaderIndex];
    if (!shader->reset()) {
        cout << "ERROR: shader->reset()" << endl;
    }

    removeParameters();
    addParameters();
    
    renderer->setProgram(shader->getProgram());
}

void ControlPanel::compileFunction() {
    DynamicShaderPtr shader = dynamicShaders[selectedShaderIndex];
    shader->setEqationU(strU);
    shader->setEqationV(strV);
    if (shader->compile()) {
        copyCDStringToClient(&statusText, COMPILATION_OK);

        renderer->setProgram(shader->getProgram());
        shader->refreshParameters();
    } else {
        copyCDStringToClient(&statusText, COMPILATION_FAILED);
    }
}

bool ControlPanel::loadBackgroundImages() {
    if (!getImagePathList("images/", backgroudImagesVector)) {
        return false;
    }

    if (backgroudImagesVector.size() == 0) {
        cout << "ERROR: There are no image files in 'images' direcotry!!" << endl;
        return false;
    }

    size_t size = backgroudImagesVector.size();

    TwEnumVal* imagesEnum = new TwEnumVal[size];
    string** filenames = new string*[size];
    int i = 0;

    for (vector<path>::iterator iter = backgroudImagesVector.begin();
            iter != backgroudImagesVector.end();) {
        path p = *(iter++);

        imagesEnum[i].Value = i;
        filenames[i] = new string(p.filename());
        imagesEnum[i].Label = filenames[i]->c_str();
        i++;
    }


    TwType imagesType = TwDefineEnum("BackgroundType", imagesEnum, size);
    TwAddVarCB(settingsBar, "Background", imagesType, setBackgroundCallback,
            getBackgroundCallback, this, "label='Image' group='Control' ");

    for (int i = 0; i < size; i++) {
        delete filenames[i];
    }

    delete[] imagesEnum;
    delete[] filenames;

   string filename = backgroudImagesVector[0].file_string();

    if (!renderer->changeTexture(filename.c_str())) {
        return false;
    }

    return true;
}

bool ControlPanel::getImagePathList(const char* directory, vector<path>& result) {
    path p(directory);

    static string extensions[] = {".bmp", ".tga", ".png", ".jpg", ".jpeg"};
    static size_t extensionsSize = sizeof (extensions) / sizeof (string);

    try {
        if (exists(p)) {

            if (is_directory(p)) {

                for (directory_iterator iter = directory_iterator(p);
                        iter != directory_iterator();) {
                    path p1 = *(iter++);

                    if (!is_regular_file(p1)) {
                        continue;
                    }

                    for (int i = 0; i < extensionsSize; i++) {
                        if (boost::iequals(extensions[i], p1.extension())) {
                            result.push_back(p1);
                            break;
                        }
                    }
                }
            } else {
                cout << "'" << p << "' is not a directory!!" << endl;
                return false;
            }

        } else {
            cout << "'" << p << "' does not exist!!" << endl;
            return false;
        }
    } catch (const filesystem_error& ex) {
        cout << ex.what() << '\n';
        return false;
    }

    return true;
}

void ControlPanel::updateCallback(float msTime) {
    if (paused) {
        return;
    }
    totalTime += msTime * speed;
    renderer->setTime(totalTime);
    refresh();
}

void TW_CALL ControlPanel::setAALevelCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->renderer->setAALevel(*(const int*) value);
}

void TW_CALL ControlPanel::getAALevelCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(int*) value = controlPanel->renderer->getAALevel();
}

void TW_CALL ControlPanel::setAAScaleCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->renderer->setAAScale(*(const float*) value);
}

void TW_CALL ControlPanel::getAAScaleCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->renderer->getAAScale();
}

void TW_CALL ControlPanel::setBackgroundCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->selectedBackgroundIndex = *(const int*) value;


    const char* filename =
            controlPanel->backgroudImagesVector[controlPanel->selectedBackgroundIndex].file_string().c_str();

    controlPanel->renderer->changeTexture(filename);
}

void TW_CALL ControlPanel::getBackgroundCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(int*) value = controlPanel->selectedBackgroundIndex;
}

void TW_CALL ControlPanel::setFunctionListCallback(const void* value, void* clientData) {
    ControlPanel* cp = static_cast<ControlPanel*> (clientData);

    const int selectedIndex = *(const int*) value;

    if (selectedIndex == cp->selectedShaderIndex) {
        return;
    }

    cp->removeParameters();
    //cout << "selected index = " << selectedIndex << endl;
    cp->selectedShaderIndex = selectedIndex;
    cp->addParameters();

    DynamicShaderPtr shader = cp->dynamicShaders[cp->selectedShaderIndex];
    cp->renderer->setProgram(shader->getProgram());

    shader->refreshParameters();
    
   // cp->totalTime = 0.0f;
}

void TW_CALL ControlPanel::getFunctionListCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(int*) value = controlPanel->selectedShaderIndex;
}

void TW_CALL ControlPanel::setParameterCallback(const void* value, void* clientData) {
    ControlParamPair* cpp = static_cast<ControlParamPair*> (clientData);

    cpp->param->value = *(const float*) value;

    cpp->control->updateFunction();
}

void TW_CALL ControlPanel::getParameterCallback(void* value, void* clientData) {
    ControlParamPair* cpp = static_cast<ControlParamPair*> (clientData);

    *(float*) value = cpp->param->value;
}

void TW_CALL ControlPanel::setXMinCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->renderer->setXMin(*(const float*) value);
}

void TW_CALL ControlPanel::getXMinCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->renderer->getXMin();
}

void TW_CALL ControlPanel::setXMaxCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->renderer->setXMax(*(const float*) value);
}

void TW_CALL ControlPanel::getXMaxCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->renderer->getXMax();
}

void TW_CALL ControlPanel::setYMinCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->renderer->setYMin(*(const float*) value);
}

void TW_CALL ControlPanel::getYMinCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->renderer->getYMin();
}

void TW_CALL ControlPanel::setYMaxCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->renderer->setYMax(*(const float*) value);
}

void TW_CALL ControlPanel::getYMaxCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->renderer->getYMax();
}

void ControlPanel::updateFunction() {
    DynamicShaderPtr shader = dynamicShaders[selectedShaderIndex];
    shader->refreshParameters();
}

void ControlPanel::resetBoundaries() {
    renderer->setXMin(-1.0f);
    renderer->setXMax(1.0f);
    renderer->setYMin(-1.0f);
    renderer->setYMax(1.0f);
}

// ---------------------------------------------------------------------------
// 2) Callback functions for C-Dynamic string variables
// ---------------------------------------------------------------------------

// Function called to copy the content of a C-Dynamic String (src) handled by
// the AntTweakBar library to a C-Dynamic string (*destPtr) handled by our application

void TW_CALL ControlPanel::copyCDStringToClient(char** destPtr, const char* src) {
    size_t srcLen = (src != NULL) ? strlen(src) : 0;
    size_t destLen = (*destPtr != NULL) ? strlen(*destPtr) : 0;

    // Alloc or realloc dest memory block if needed
    if (*destPtr == NULL)
        *destPtr = (char *) malloc(srcLen + 1);
    else if (srcLen > destLen)
        *destPtr = (char *) realloc(*destPtr, srcLen + 1);

    // Copy src
    if (srcLen > 0)
        strncpy(*destPtr, src, srcLen);
    (*destPtr)[srcLen] = '\0'; // null-terminated string
}

void TW_CALL ControlPanel::resetFuncParamsButtonCallback(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    controlPanel->resetFunction();
}

void TW_CALL ControlPanel::resetBoundariesCallback(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    controlPanel->resetBoundaries();
}

void TW_CALL ControlPanel::resetTimeCallback(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    controlPanel->totalTime = 0.0f;
}

void TW_CALL ControlPanel::compileButtonCallback(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    controlPanel->compileFunction();
}

