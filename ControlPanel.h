/* 
 * File:   ControlPanel.h
 * Author: Revers
 *
 * Created on 26 kwiecień 2012, 20:33
 */

#ifndef CONTROLPANEL_H
#define	CONTROLPANEL_H

#include <GL/glfw.h>
#include <AntTweakBar.h>

#include <vector>
#include <boost/filesystem.hpp>

#include "ConfXMLFunctions.h"
#include "DynamicShader.h"

#include "Renderer.h"

class ControlPanel;

struct ControlParamPair {
    ControlPanel* control;
    DynamicShaderParam* param;

    ControlParamPair(ControlPanel* control_,
            DynamicShaderParam * param_) : control(control_), param(param_) {
    }
};

class ControlPanel {
    typedef std::vector<DynamicShaderPtr> DynamicShadersVector;
    typedef boost::shared_ptr<ControlParamPair> ControlParamPairPtr;
    typedef std::vector<ControlParamPairPtr> ControlParamPairVector;

    static const char* COMPILATION_OK;
    static const char* COMPILATION_FAILED;

    struct TweakBarBounds {
        int x;
        int y;
        int width;
        int height;
    };

    TweakBarBounds settingsBarBounds;
    TwBar* settingsBar;

    ConfXMLPtr xmlFunctions;
    RendererPtr renderer;
    float totalTime;

    bool paused;

    DynamicShadersVector dynamicShaders;
    ControlParamPairVector controlParamPairVector;
    int selectedShaderIndex;

    bool parametersAdded;
    char* strU;
    char* strV;
    char* statusText;
public:
    bool mouseOver;
    float speed;

    std::vector<boost::filesystem::path> backgroudImagesVector;
    int selectedBackgroundIndex;

    ControlPanel() {
        parametersAdded = false;

        strU = NULL;
        strV = NULL;

        statusText = NULL;
        copyCDStringToClient(&statusText, COMPILATION_OK);

        //        copyCDStringToClient(&strU, "uquation u");
        //      copyCDStringToClient(&strV, "uquation v");

        selectedShaderIndex = 0;
        selectedBackgroundIndex = 0;
        totalTime = 0.0f;
        settingsBar = NULL;
        mouseOver = false;
        speed = 0.1f;

        paused = true;
    }

    virtual ~ControlPanel() {
        TwTerminate();
    }

    bool init(RendererPtr renderer, ConfXMLPtr xmlFunctions);

    void draw() {
        TwDraw();
    }

    void reset() {
        totalTime = 0.0f;
    }

    void updateCallback(float msTime);

    void keyDownCallback(int key, int action) {
        TwEventKeyGLFW(key, action);

        if (action != GLFW_PRESS) {
            return;
        }

        if (key == GLFW_KEY_ENTER) {
            compileFunction();
        }
    }

    void resizeCallback(int width, int height) {
        if (renderer) {
            renderer->windowSizeChanged(width, height);
        }
        TwWindowSize(width, height);
    }

    void mousePosCallback(int x, int y) {
        TwEventMousePosGLFW(x, y);
    }

    void mouseButtonCallback(int id, int state) {
        TwGetParam(settingsBar, NULL, "size", TW_PARAM_INT32, 2, &settingsBarBounds.width);
        TwGetParam(settingsBar, NULL, "position", TW_PARAM_INT32, 2, &settingsBarBounds.x);

        if (id == GLFW_MOUSE_BUTTON_LEFT) {
            if (state == GLFW_PRESS) {
                int x, y;
                glfwGetMousePos(&x, &y);

                if (x >= settingsBarBounds.x
                        && x < settingsBarBounds.x + settingsBarBounds.width
                        && y >= settingsBarBounds.y
                        && y < settingsBarBounds.y + settingsBarBounds.height) {
                    mouseOver = true;
                }
            } else {
                mouseOver = false;
            }
        }

        TwEventMouseButtonGLFW(id, state);
    }

    void mouseWheelCallback(int pos) {
        refresh();
        TwEventMouseWheelGLFW(pos);
    }

    void refresh() {
        TwRefreshBar(settingsBar);
    }

private:

    static void TW_CALL setSpeedCallback(const void* value, void* clientData);
    static void TW_CALL getSpeedCallback(void* value, void* clientData);

    static void TW_CALL setParamACallback(const void* value, void* clientData);
    static void TW_CALL getParamACallback(void* value, void* clientData);

    static void TW_CALL setAALevelCallback(const void* value, void* clientData);
    static void TW_CALL getAALevelCallback(void* value, void* clientData);

    static void TW_CALL setAAScaleCallback(const void* value, void* clientData);
    static void TW_CALL getAAScaleCallback(void* value, void* clientData);

    static void TW_CALL setBackgroundCallback(const void* value, void* clientData);
    static void TW_CALL getBackgroundCallback(void* value, void* clientData);

    static void TW_CALL setFunctionListCallback(const void* value, void* clientData);
    static void TW_CALL getFunctionListCallback(void* value, void* clientData);

    static void TW_CALL setParameterCallback(const void* value, void* clientData);
    static void TW_CALL getParameterCallback(void* value, void* clientData);

    static void TW_CALL setXMinCallback(const void* value, void* clientData);
    static void TW_CALL getXMinCallback(void* value, void* clientData);

    static void TW_CALL setXMaxCallback(const void* value, void* clientData);
    static void TW_CALL getXMaxCallback(void* value, void* clientData);

    static void TW_CALL setYMinCallback(const void* value, void* clientData);
    static void TW_CALL getYMinCallback(void* value, void* clientData);

    static void TW_CALL setYMaxCallback(const void* value, void* clientData);
    static void TW_CALL getYMaxCallback(void* value, void* clientData);

    static void TW_CALL copyCDStringToClient(char** destPtr, const char* src);

    static void TW_CALL resetTimeCallback(void* clientData);
    static void TW_CALL resetBoundariesCallback(void* clientData);
    static void TW_CALL resetFuncParamsButtonCallback(void* clientData);
    static void TW_CALL compileButtonCallback(void* clientData);

    bool loadBackgroundImages();

    bool getImagePathList(const char* directory,
            std::vector<boost::filesystem::path>& result);

    bool loadDynamicShaders();

    void addParameters();
    void removeParameters();

    void updateFunction();

    void resetFunction();
    
    void resetBoundaries();

    void compileFunction();
};

#endif	/* CONTROLPANEL_H */

