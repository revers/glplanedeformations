/* 
 * File:   DynamicShader.cpp
 * Author: Revers
 * 
 * Created on 14 maj 2012, 19:12
 */

#include <iostream>
#include <sstream>

#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>

#include "RevUtil.hpp"

#include "DynamicShader.h"

#define VARIABLES_PART "//<VARIABLES>"
#define EQUATIONS_PART "//<EQUATIONS>"
#define FAKE_INCLUDE_PART "//$END_FAKE$"

#define VERTEX_SHADER "//-- Vertex"
#define FRAGMENT_SHADER "//-- Fragment"
#define GEOMETRY_SHADER "//-- Geometry"
#define TESS_CONTROL_SHADER "//-- TessControl"
#define TESS_EVAL_SHADER "//-- TessEval"

#define TEMPLATE_SHADER_FILE "shaders/TEMPLATE_Renderer.glsl"

using namespace std;

string DynamicShader::templateGLSL;

bool DynamicShader::init() {
    if (templateGLSL.empty()) {
        if (!RevUtil::readFile(TEMPLATE_SHADER_FILE, templateGLSL)) {
            return false;
        }
    }

    return reset();
}

bool DynamicShader::reset() {
    equationU = xmlFunction->getU();
    equationV = xmlFunction->getV();

    parameters.clear();

    GList* parameterList = xmlFunction->getParameterList();
    GListIterator iter(parameterList);

    while (iter()) {
        ConfXMLParameter* xmlParameter = static_cast<ConfXMLParameter*> (iter++);
        parameters.push_back(DynamicShaderParam(xmlParameter->getValue(), xmlParameter));
    }

    return compile();
}

void DynamicShader::formatEquation(string& str) {
    str.insert(0, " ");
    str.append(" ");

    static boost::regex regx;

    regx.assign("([^A-Za-z])t([^A-Za-z])", boost::regex::icase);
    str = boost::regex_replace(str, regx, "\\1Time\\2");

    regx.assign("([^A-Za-z])s([^A-Za-z])", boost::regex::icase);
    str = boost::regex_replace(str, regx, "\\1Speed\\2");

    regx.assign("([^A-Za-z])a([^A-Za-z])", boost::regex::icase);
    str = boost::regex_replace(str, regx, "\\1angle\\2");

    regx.assign("([^A-Za-z])r([^A-Za-z])", boost::regex::icase);
    str = boost::regex_replace(str, regx, "\\1radius\\2");

    regx.assign("([^A-Za-z])pi([^A-Za-z])", boost::regex::icase);
    str = boost::regex_replace(str, regx, "\\13.1415927f\\2");

    regx.assign("([^A-Za-z])e([^A-Za-z])", boost::regex::icase);
    str = boost::regex_replace(str, regx, "\\12.7182817f\\2");
    
    regx.assign("([^A-Za-z])x([^A-Za-z])", boost::regex::icase);
    str = boost::regex_replace(str, regx, "\\1coords.x\\2");
    
    regx.assign("([^A-Za-z])y([^A-Za-z])", boost::regex::icase);
    str = boost::regex_replace(str, regx, "\\1coords.y\\2");

    boost::trim(str);
}

bool DynamicShader::compile() {
    cout << "Compiling function '" << xmlFunction->getName() << "'..." << endl;
    if (templateGLSL.empty()) {
        std::cout << "ERROR: templateGLSL.empty() == TRUE!!" << std::endl;
        return false;
    }

    string newU = equationU;
    string newV = equationV;

    formatEquation(newU);
    formatEquation(newV);

    newU.insert(0, "float u = ");
    newU.append(";\n    float v = ");
    newU.append(newV).append(";\n");

   // cout << newU;

    string shaderFile = templateGLSL;
    boost::replace_first(shaderFile, EQUATIONS_PART, newU);

    string variables;
    for (int i = 0; i < parameters.size(); i++) {
        DynamicShaderParam* param = &parameters[i];
        variables.append("uniform float ");
        variables.append(param->xmlParameter->getShortName()).append(";\n");
    }

    boost::replace_first(shaderFile, VARIABLES_PART, variables);
    shaderFile.append("\n\n");

   // cout << "----------------------------------------------\n"
   //         << shaderFile
   //         << "\n----------------------------------------------\n";

    GLSLProgramPtr program(new GLSLProgram);

    if (!program->compileShaderGLSLString(shaderFile)) {
        cout << xmlFunction->getName() << " shader compilation FAILED!" << endl;
        return false;
    }
    
    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        cout << xmlFunction->getName() << " shader linking FAILED!" << endl;
        return false;
    }
    
    this->program = program;
    
    cout << "Function '" << xmlFunction->getName() << "' compilation  succeed." << endl;
    
    refreshParameters();

    return true;
}

void DynamicShader::refreshParameters() {
    program->use();
    
    for(DynamicShaderParameters::iterator iter = parameters.begin(); 
            iter != parameters.end(); ++iter) {
        const char* name = iter->xmlParameter->getShortName();
        float value = iter->value;
        program->setUniform(name, value);
    }
}