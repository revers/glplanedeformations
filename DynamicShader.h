/* 
 * File:   DynamicShader.h
 * Author: Revers
 *
 * Created on 14 maj 2012, 19:12
 */

#ifndef DYNAMICSHADER_H
#define	DYNAMICSHADER_H


#include <vector>
#include <string>

#include <boost/shared_ptr.hpp>

#include "ConfXMLFunction.h"
#include "ConfXMLParamter.h"

#include "glslprogram.h"

typedef boost::shared_ptr<GLSLProgram> GLSLProgramPtr;

struct DynamicShaderParam {
    float value;
    ConfXMLParameter* xmlParameter;

    DynamicShaderParam() : value(0.0f), xmlParameter(NULL) {
    }

    DynamicShaderParam(float value_, ConfXMLParameter * xmlParameter_) :
    value(value_), xmlParameter(xmlParameter_) {
    }
};

typedef std::vector<DynamicShaderParam> DynamicShaderParameters;

class DynamicShader {
    std::string equationU;
    std::string equationV;
    DynamicShaderParameters parameters;
    ConfXMLFunction* xmlFunction;
    GLSLProgramPtr program;

    static std::string templateGLSL;

public:

    DynamicShader(ConfXMLFunction* xmlFunction_) :
    xmlFunction(xmlFunction_) {
        program = GLSLProgramPtr(new GLSLProgram);
    }

    virtual ~DynamicShader() {
    }

    const char* getName() {
        return xmlFunction->getName();
    }

    bool init();

    bool reset();

    bool compile();

    void setParameter(size_t index, float value) {
        parameters[index].value = value;

        program->use();
        program->setUniform(parameters[index].xmlParameter->getName(), value);
    }

    float getParameter(size_t index) {
        return parameters[index].value;
    }
    
    void setEqationU(const char* newEquationU) {
        this->equationU = newEquationU;
    }

    void setEqationV(const char* newEquationV) {
        this->equationV = newEquationV;
    }

    void setEqationU(std::string& newEquationU) {
        this->equationU = newEquationU;
    }

    void setEqationV(std::string& newEquationV) {
        this->equationV = newEquationV;
    }

    const char* getEquationU() const {
        return equationU.c_str();
    }

    const char* getEquationV() const {
        return equationV.c_str();
    }

    ConfXMLFunction* getXmlFunction() const {
        return xmlFunction;
    }

    DynamicShaderParameters& getParameters() {
        return parameters;
    }

    GLSLProgramPtr getProgram() {
        return program;
    }

    void refreshParameters();
    //private:
    void formatEquation(std::string& str);
};

typedef boost::shared_ptr<DynamicShader> DynamicShaderPtr;
#endif	/* DYNAMICSHADER_H */

