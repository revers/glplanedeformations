/* 
 * File:   MyCLFractals.cpp
 * Author: Revers
 *
 * Created on 17 marzec 2012, 15:54
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <iostream>

#include <GL/glew.h>
#include <GL/glfw.h>
#include <GL/gl.h>

#include <SOIL.h>

#include "ControlPanel.h"

#include "FPSCounter.h"
#include "Renderer.h"

#include <XMLFoundation.h>
#include "ConfXMLParamter.h"
#include "ConfXMLFunction.h"
#include "ConfXMLFunctions.h"

#include "DynamicShader.h"

using namespace std;

int running = 1;


//------------------------------------------------
// Defines:
//------------------------------------------------
#define APP_TITLE "GLPlaneDeformations"

#define FAILURE 1
#define SUCCESS 0

#define WINDOW_WIDTH 1200
#define WINDOW_HEIGHT 600

#define SCREENSHOT_FILENAME "images/screenshot.bmp"

//------------------------------------------------
// Globals:
//------------------------------------------------
bool lmbPressed = false;
int lastMouseWheelPos = 0;
int lastX = 0;
int lastY = 0;
int windowWidth = 0;
int windowHeight = 0;

bool wireFrameMode = false;

FPSCounter fpsCounter(1.0f / 60.0f);
RendererPtr renderer(new Renderer(WINDOW_WIDTH, WINDOW_HEIGHT));
ControlPanel controlPanel;

void renderScene() {
    glClear(GL_COLOR_BUFFER_BIT);

    if (wireFrameMode) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    renderer->render();

    controlPanel.draw();

    glfwSwapBuffers();
}

void takeScreenshot() {
    int succ = SOIL_save_screenshot(SCREENSHOT_FILENAME,
            SOIL_SAVE_TYPE_BMP, 0, 0, windowWidth, windowHeight);
    if (succ) {
        cout << "obrazek " << SCREENSHOT_FILENAME << " zapisany pomyslnie :)" << endl;
    } else {
        cout << "Nie udalo sie zapisac obrazka " << SCREENSHOT_FILENAME << " :(" << endl;
    }

}

void GLFWCALL keyDownCallback(int key, int action) {
    controlPanel.keyDownCallback(key, action);

    if (action != GLFW_PRESS) {
        return;
    }

    switch (key) {
        case GLFW_KEY_ESC:
            running = 0;
            break;
        case GLFW_KEY_SPACE:
            // wireFrameMode = !wireFrameMode;
            //  takeScreenshot();
            break;

        case 'a':
        case 'A':
        {

        }
            break;

        default:
            break;
    }
}

void GLFWCALL resizeCallback(int width, int height) {
    controlPanel.resizeCallback(width, height);
    windowWidth = width;
    windowHeight = height;
    float ratio = 1.0f;

    if (height > 0) {
        ratio = (float) width / (float) height;
    }

    glViewport(0, 0, width, height);

    //    renderer->windowSizeChanged(width, height);
}

void GLFWCALL mousePosCallback(int x, int y) {

    controlPanel.mousePosCallback(x, y);

    if (lmbPressed) {

        int xDiff = lastX - x;
        int yDiff = lastY - y;

        controlPanel.refresh();
        renderer->move(xDiff, yDiff);

        lastX = x;
        lastY = y;
    }
}

void GLFWCALL mouseButtonCallback(int id, int state) {

    controlPanel.mouseButtonCallback(id, state);

    if (id == GLFW_MOUSE_BUTTON_LEFT) {
        if (state == GLFW_PRESS) {
            int x, y;
            glfwGetMousePos(&x, &y);

            lastX = x;
            lastY = y;
            if (!controlPanel.mouseOver) {
                lmbPressed = true;
            }
        } else {
            lmbPressed = false;
        }
    }
}

void GLFWCALL mouseWheelCallback(int pos) {
    controlPanel.mouseWheelCallback(pos);

    int diff = pos - lastMouseWheelPos;
    lastMouseWheelPos = pos;

    if (diff > 0) {
        renderer->zoomIn();
    } else if (diff < 0) {
        renderer->zoomOut();
    }
}

int initializeGL() {

    //glEnable(GL_MULTISAMPLE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glClearColor(0, 0, 0, 1);

    return SUCCESS;
}

void testConfXML() {
    try {
        ConfXMLFunctions xmlRoot;

        xmlRoot.FromXMLFile("functions.xml");
        printf(xmlRoot.ToXML());
        cout << "\n\n";

        GList* functionList = xmlRoot.getFunctionList();
        GListIterator iter(functionList);

        int i = 0;
        while (iter()) {
            ConfXMLFunction* xmlFunction = static_cast<ConfXMLFunction*> (iter++);
            cout << (++i) << ") " << xmlFunction->getName() << endl;
        }

        xmlRoot.ToXMLFile("test.xml");

    } catch (GException &rErr) {
        printf(rErr.GetDescription());
    }
}

void testDynamicShader() {
    try {
        ConfXMLFunctions xmlRoot;

        xmlRoot.FromXMLFile("functions.xml");

        ConfXMLFunction* function =
                static_cast<ConfXMLFunction*> (xmlRoot.getFunctionList()->First());
        cout << "Function name = " << function->getName() << endl;

        DynamicShader dynamicShader(function);

        if (!dynamicShader.init()) {
            cout << "DynamicShader::init() FAILED!!" << endl;
        }

        string equation = "sin(frequency * (a + t * s)) / r";
        cout << "Eqaution = '" << equation << "'" << endl;

        dynamicShader.formatEquation(equation);

        cout << "Eqaution = '" << equation << "'" << endl;
    } catch (GException &rErr) {
        printf(rErr.GetDescription());
    }
}

ConfXMLPtr getConfXMLFunctions() {
    try {
        ConfXMLPtr xmlPtr(new ConfXMLFunctions);

        xmlPtr->FromXMLFile("functions.xml");

        const char* cc = xmlPtr->ToXML();
        cout << cc << endl;;
        return xmlPtr;
    } catch (GException &rErr) {
        printf(rErr.GetDescription());
    }

    return ConfXMLPtr();
}

int main(int argc, char* argv[]) {
    srand(time(0));

    //testConfXML();

    if (glfwInit() == GL_FALSE) {
        fprintf(stderr, "GLFW initialization failed\n");
        exit(-1);
    }

    int width = WINDOW_WIDTH;
    int height = WINDOW_HEIGHT;

    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 2);
    glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwOpenWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    //glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);


    GLFWvidmode mode;
    glfwGetDesktopMode(&mode);

    if (glfwOpenWindow(width, height, mode.RedBits, mode.GreenBits, mode.BlueBits,
            0, 1, 0, GLFW_WINDOW /* or GLFW_FULLSCREEN */) == GL_FALSE) {
        fprintf(stderr, "Could not open window\n");
        glfwTerminate();
        exit(-1);
    }

    glewInit();

    glfwSetWindowTitle(APP_TITLE);
    glfwSwapInterval(1);

    glfwSetKeyCallback(keyDownCallback);
    glfwSetMousePosCallback(mousePosCallback);
    glfwSetMouseButtonCallback(mouseButtonCallback);
    glfwSetMouseWheelCallback(mouseWheelCallback);
    glfwSetWindowSizeCallback(resizeCallback);
    glfwSetCharCallback((GLFWcharfun) TwEventCharGLFW);


    glfwEnable(GLFW_KEY_REPEAT);

    if (initializeGL() == FAILURE) {
        cout << "ERROR: initializeGL FAILED!" << endl;
        return -1;
    }

    if (renderer->init() == false) {
        cout << "ERROR: fractalRnederer.init() FAILED!" << endl;
        return -1;
    }

    ConfXMLPtr confXml = getConfXMLFunctions();
    if (!confXml) {
        cout << "ERROR: getConfXMLFunctions(); FAILED!" << endl;
        return -1;
    }

    if (controlPanel.init(renderer, confXml) == false) {
        cout << "ERROR: ControlPanel::init() FAILED!";
        return -1;
    }

    glfwSwapInterval(0);

    int glVersion[2] = {-1, -1};
    glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]);
    glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]);

    std::cout << "Using OpenGL: " << glVersion[0] << "." << glVersion[1] << std::endl;

    GLint bufs, samples;
    glGetIntegerv(GL_SAMPLE_BUFFERS, &bufs);
    glGetIntegerv(GL_SAMPLES, &samples);
    cout << "MSAA: buffers = " << bufs << " samples = " << samples << "\n" << endl;

    //testDynamicShader();

    while (running) {

        fpsCounter.frameBegin();

        renderScene();

        running = running && glfwGetWindowParam(GLFW_OPENED);

        if (fpsCounter.frameEnd()) {
            controlPanel.updateCallback(fpsCounter.getTimeFromLastNotification());

            char title[256];
            sprintf(title, APP_TITLE " | %d fps ", fpsCounter.getFramesPerSec());

            glfwSetWindowTitle(title);
        }
    }

    glfwTerminate();

    return 0;
}


