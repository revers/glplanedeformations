/* 
 * File:   Renderer.cpp
 * Author: Revers
 * 
 * Created on 8 maj 2012, 21:55
 */

#include <iostream>
#include <SOIL.h>
#include "Renderer.h"
#include "GL/gl.h"
#include "GL/glext.h"
#include "GL/glew.h"

using namespace std;

//#define TEXTURE_FILE "images/test.png"

#define X_MIN -0.4f
#define X_MAX 1.0f
#define Y_MIN -1.0f
#define Y_MAX 1.0f

Renderer::Renderer(int width_, int height_)
: quadVAO(0), texture(0),
width(width_), height(height_) {
    program = GLSLProgramPtr();
    time = 0.0f;

    zoomFactor = 1.3;
    aaLevel = 3;
    aaScale = 0.006f;

    xMin = -1.0f;
    xMax = 1.0f;
    yMin = -1.0f;
    yMax = 1.0f;

    windowSizeChanged(width_, height_);
}

void Renderer::windowSizeChanged(int newWidth, int newHeight) {
    //cout << "windowSizeChanged(" << newWidth << ", " << newHeight << ")" << endl;

    width = newWidth;
    height = newHeight;
    
    imageWidth = (X_MAX - X_MIN) / 2.0f * (float) width;
    imageHeight = (float)height;

    calculateBoundaries();
}

void Renderer::render() {

    if (!program || !texture)
        return;

    program->use();
    glEnable(GL_TEXTURE_2D);

    glActiveTexture(GL_TEXTURE0);

    glBindTexture(GL_TEXTURE_2D, texture);

    glBindVertexArray(quadVAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
}

bool Renderer::init() {

    createQuadVAO();

    if (program) {
        resetParameters();
    }

    return true;
}

void Renderer::move(int shiftX, int shiftY) {
    float onePixelX = imageVirtualWidth / imageWidth;
    float onePixelY = imageVirtualHeight / imageHeight;
    
    float shiftVirutalX = onePixelX * (float) shiftX;
    float shiftVirutalY = onePixelY * (float) shiftY;
    
    xMin += shiftVirutalX;
    xMax += shiftVirutalX;
    
    yMin += shiftVirutalY;
    yMax += shiftVirutalY;
    
    setBoundaries();
}

void Renderer::zoomIn() {
    float imgWidth = xMax - xMin;
    float imgHeight = yMax - yMin;

    float newDistX = imgWidth / zoomFactor;
    float newDistY = imgHeight / zoomFactor;

    float offsetX = (imgWidth - newDistX) / 2.0;
    float offsetY = (imgHeight - newDistY) / 2.0;

    xMin += offsetX;
    yMin += offsetY;

    xMax -= offsetX;
    yMax -= offsetY;

    calculateBoundaries();
    setBoundaries();
}

void Renderer::zoomOut() {
    float imgWidth = xMax - xMin;
    float imgHeight = yMax - yMin;

    float newDistX = imgWidth * zoomFactor;
    float newDistY = imgHeight * zoomFactor;

    float offsetX = (newDistX - imgWidth) / 2.0;
    float offsetY = (newDistY - imgHeight) / 2.0;

    xMin -= offsetX;
    yMin -= offsetY;

    xMax += offsetX;
    yMax += offsetY;

    calculateBoundaries();
    setBoundaries();
}

void Renderer::createQuadVAO() {

    GLfloat verts[] = {
        X_MIN, Y_MIN, 0.0f,
        X_MAX, Y_MIN, 0.0f,
        X_MAX, Y_MAX, 0.0f,
        X_MIN, Y_MIN, 0.0f,
        X_MAX, Y_MAX, 0.0f,
        X_MIN, Y_MAX, 0.0f
    };
    GLfloat tc[] = {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f
    };

    unsigned int handle[2];
    glGenBuffers(2, handle);

    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glBufferData(GL_ARRAY_BUFFER, 6 * 3 * sizeof (float), verts, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glBufferData(GL_ARRAY_BUFFER, 6 * 2 * sizeof (float), tc, GL_STATIC_DRAW);

    // Set up the vertex array object

    glGenVertexArrays(1, &quadVAO);
    glBindVertexArray(quadVAO);

    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glVertexAttribPointer((GLuint) 0, 3, GL_FLOAT, GL_FALSE, 0, ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(0); // Vertex position

    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glVertexAttribPointer((GLuint) 1, 2, GL_FLOAT, GL_FALSE, 0, ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(1); // Texture coordinates

    glBindVertexArray(0);
}

GLuint loadTexture(const char * filename, int* width = NULL, int* height = NULL) {
    GLuint texture = 0;

    int channels;

    int w;
    int h;
    unsigned char* data = SOIL_load_image
            (
            filename,
            &w, &h,
            &channels,
            SOIL_LOAD_RGBA);

    if (!data) {
        return 0;
    }

    if (width) {
        *width = w;
    }

    if (height) {
        *height = h;
    }


    glGenTextures(1, &texture); //generate the texture with the loaded data
    glBindTexture(GL_TEXTURE_2D, texture); //bind the texture to it's array
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    //GL_MODULATE ); //set texture environment parameters

    //And if you go and use extensions, you can use Anisotropic filtering textures which are of an
    //even better quality, but this will do for now.
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //Here we are setting the parameter to repeat the texture instead of clamping the texture
    //to the edge of our shape. 
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    //Generate the texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

    SOIL_free_image_data(data);
    //free( data ); //free the texture

    return texture;
}

bool Renderer::changeTexture(const char* textureFile) {

    GLuint newTexture = loadTexture(textureFile);

    if (!newTexture) {
        cout << "Loading texture '" << textureFile << "' FAILED!" << endl;
        return false;
    }

    if (!texture) {
        glDeleteTextures(1, &texture);
    }

    texture = newTexture;
    cout << "Texture '" << textureFile << "' loaded successfully." << endl;

    return true;
}

void Renderer::calculateBoundaries() {
    imageVirtualWidth = (xMax - xMin);
    imageVirtualHeight = (yMax - yMin);
}

void Renderer::setBoundaries() {
    program->use();

    program->setUniform("XMin", xMin);
    program->setUniform("YMax", yMax);
    program->setUniform("ImageVirtualWidth", imageVirtualWidth);
    program->setUniform("ImageVirtualHeight", imageVirtualHeight);
}
