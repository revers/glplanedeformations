/* 
 * File:   Renderer.h
 * Author: Revers
 *
 * Created on 8 maj 2012, 21:55
 */

#ifndef RENDERER_H
#define	RENDERER_H

#include <boost/shared_ptr.hpp>
#include "glslprogram.h"

using glm::vec2;
using glm::vec3;

typedef boost::shared_ptr<GLSLProgram> GLSLProgramPtr;

class Renderer {
    GLuint quadVAO;
    GLuint texture;
    GLSLProgramPtr program;

    int width;
    int height;
    float time;

    int aaLevel;
    float aaScale;

    float xMin;
    float xMax;
    float yMin;
    float yMax;

    float imageWidth;
    float imageHeight;
    float imageVirtualWidth;
    float imageVirtualHeight;

    float zoomFactor;

public:

    Renderer(int width_, int height_);

    virtual ~Renderer() {
    }

    void render();

    bool init();

    void move(int shiftX, int shiftY);

    void zoomIn();

    void zoomOut();

    void windowSizeChanged(int newWidth, int newHeight);

    void createQuadVAO();

    float getTime() const {
        return time;
    }

    void setTime(float time) {
        this->time = time;
        program->use();
        program->setUniform("Time", time);
    }

    int getAALevel() const {
        return aaLevel;
    }

    void setAALevel(int aaLevel) {
        this->aaLevel = aaLevel;

        program->use();
        program->setUniform("AALevel", aaLevel);
        program->setUniform("InvAALevel", 1.0f / (float) aaLevel);
    }

    float getAAScale() const {
        return aaScale;
    }

    void setAAScale(float aaScale) {
        this->aaScale = aaScale;

        program->use();
        program->setUniform("AAScale", aaScale);
    }

    float getXMax() const {
        return xMax;
    }

    void setXMax(float xMax) {

        this->xMax = xMax;

        calculateBoundaries();
        setBoundaries();
    }

    float getXMin() const {

        return xMin;
    }

    void setXMin(float xMin) {

        this->xMin = xMin;

        calculateBoundaries();
        setBoundaries();
    }

    float getYMax() const {

        return yMax;
    }

    void setYMax(float yMax) {
        this->yMax = yMax;

        calculateBoundaries();
        setBoundaries();
    }

    float getYMin() const {

        return yMin;
    }

    void setYMin(float yMin) {
        this->yMin = yMin;

        calculateBoundaries();
        setBoundaries();
    }

    void resetParameters() {
        setAALevel(aaLevel);
        setAAScale(aaScale);

        setBoundaries();
    }

    void setProgram(GLSLProgramPtr program) {
        this->program = program;

        time = 0.0f;
        setTime(time);
        setAALevel(aaLevel);
        setAAScale(aaScale);

        setBoundaries();
    }

    bool changeTexture(const char* textureFile);

private:
    void calculateBoundaries();

    void setBoundaries();
};

typedef boost::shared_ptr<Renderer> RendererPtr;

#endif	/* RENDERER_H */

