// --------------------------------------------------------------------------
//						United Business Technologies
//			  Copyright (c) 2000 - 2009  All Rights Reserved.
//
// Source in this file is released to the public under the following license:
// --------------------------------------------------------------------------
// This toolkit may be used free of charge for any purpose including corporate
// and academic use.  For profit, and Non-Profit uses are permitted.
//
// This source code and any work derived from this source code must retain 
// this copyright at the top of each source file.
// --------------------------------------------------------------------------

#ifndef __HASH_TABLE_H__
#define __HASH_TABLE_H__

#include "GString.h"
#include "GBTree.h"

// this is a Hash to an Array of balanced Trees with a Lists, it's fast.
class GHash
{
protected:

	GBTree *m_table;
	int		m_nCount;
	int		m_nHashTableSize;
	int		m_nDeferDestruction;

	void InitTable();

	friend class GHashIterator; // GHash Iterator Class

public:

	// number of elements
	int  GetCount() const;
	bool IsEmpty() const;

	// Lookup
	void *operator[](const char *key) const;

	void *Lookup(const char *key, unsigned int nOccurance = 1) const;
	void *Lookup(unsigned int key, unsigned int nOccurance = 1) const;
	void *Lookup(void *key, unsigned int nOccurance = 1) const;


	// add a new (key, value) pair
	void Insert(const char *key, void *value);
	void Insert(unsigned int key, void *value);
	void Insert(void *key, void *value);

	// if you use a unique key, this will always return 0 or 1
//	unsigned int GetOccuranceCount(const char *key);
//	unsigned int GetOccuranceCount(unsigned int key);
//	unsigned int GetOccuranceCount(void *key);

	// removing existing (key, ?) pair
	// when nOccurance = 0, all occurances of the key are removed, otherwise nOccurance = must be 1 or > to remove the first or the second occurance etc..
	void *RemoveKey(unsigned int key, unsigned int nOccurance = 0);
	void *RemoveKey(const char *key, unsigned int nOccurance = 0);
	void *RemoveKey(void *key, unsigned int nOccurance = 0);
	void reassignKeyPair(const char*key, void *pOld, void*pNew);

	void RemoveAll();

	unsigned int HashKey(const char * key) const;

	void DeferDestruction();
	void Destruction();

	// Construct the Hash with a prime number.  For very large data sets you may want
	// one of these primes: 4441, 8123, 11777, 17017
	// or a smaller prime :    7, 101, 503, 1009
	GHash(unsigned int nPrime = 2503);
	~GHash();
};

class GHashIterator
{
	GBTreeIterator m_it;
	int m_itOK;

	const GHash &m_hash;
	int m_nHashIndex;
	void *m_pLookAhead;
	void *Increment();
public:

	GHashIterator(const GHash *hash);
	~GHashIterator();
	int operator ()(void);
	void * operator ++ (int);
};

#endif // __HASH_TABLE_H__
