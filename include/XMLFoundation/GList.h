// --------------------------------------------------------------------------
//						United Business Technologies
//			  Copyright (c) 2000 - 2009  All Rights Reserved.
//
// Source in this file is released to the public under the following license:
// --------------------------------------------------------------------------
// This toolkit may be used free of charge for any purpose including corporate
// and academic use.  For profit, and Non-Profit uses are permitted.
//
// This source code and any work derived from this source code must retain 
// this copyright at the top of each source file.
// --------------------------------------------------------------------------

#ifndef _GLIST_H____
#define _GLIST_H____

class XMLObject;
class GList
{
public:
	// doubly linked list structure
	class Node 
	{
	public	:
		Node * PrevNode; // pointer to previous node    [ <<- | <T> |  -> ]
		void * Data;  // pointer to data
		Node * NextNode; // pointer to next node        [ <-  | <T> | ->> ]
		void AddAfter(Node *pN) 
		{
			// procedure to add 'this' node after the node 'pN'
			NextNode = pN->NextNode;
			if (pN->NextNode) 
				pN->NextNode->PrevNode = this;
			PrevNode = pN;
			pN->NextNode = this;
		};
		void AddBefore(Node *pN) 
		{
			// procedure to add 'this' node before the node 'pN'
			PrevNode = pN->PrevNode;
			if (pN->PrevNode) 
				pN->PrevNode->NextNode = this;
			NextNode = pN;
			pN->PrevNode = this;
		};
		void UnLink() 
		{
			// procedure to remove 'this' node from the list,
			// and re-link the list
			if (PrevNode) PrevNode->NextNode = NextNode;
			if (NextNode) NextNode->PrevNode = PrevNode;
		};

		void init(Node *pN, int InsertAfter = 1) 
		{
			// Node constructor -- if 'pN' is NULL, this is the first node in
			// the list.  InsertAfter is a boolean indicating where this node
			// is to be inserted, i.e. before | after the node 'pN'.
			if (pN) 
			{
				if (InsertAfter) 
					AddAfter(pN);
				else 
					AddBefore(pN);
			}
			else 
			{
				NextNode = PrevNode = 0;
			}
		}

		Node(Node *pN, int InsertAfter = 1) 
		{
			init(pN, InsertAfter);
		};
		~Node() 
		{
			// node destructor
			UnLink();
		};
	};

protected:

	Node  * FirstNode,    // First element in list
		  * LastNode,     // Last element in list
		  * CurrentNode;  // Current element in list
	int  iSize;           // number of nodes in list
	
	int nDeferDestruction;

	friend class GListIterator; // List Iterator Class

public:

	// nodal op's
	void SetCurrent(Node *p);
	Node *GetCurrent() { return CurrentNode; }

	GList(); // list constructor - initilize Node *'s
	GList(const GList &);
	virtual ~GList(void);// Calls RemoveAll for list clean-up

	void operator=(const GList &);

	void AppendList(const GList *);	// copy the contents from the parameter list
										// into the 'this' list
	void * First(void) const;
					  // function that returns a reference to the data in the
					  // first node of the list. 
	void * Last(void) const;
					  // function that returns a reference to the data in the
					  // last node of the list.  
	void * Current(void) const;
					  // function that returns a reference to the data in the
					  // current node of the list.  
	void * Next(void) const;
					  // function that returns a reference to the data in the
					  // next node of the list.  An exception is thrown if
					  // the list is empty.  The next node is the node following
					  // the current node.  The current node becomes the next
					  // node.
	void * Previous(void) const;
					  // function that returns a reference to the data in the
					  // previous node of the list.  An exception is thrown if
					  // the list is empty.  The previous node is the node before
					  // the current node.  The current node becomes the previous
					  // node.
	void * Tail(void) const;
					  // function that returns a reference to the data in the
					  // next node of the list.  An exception is thrown if
					  // the list is empty.  The next node is the node following
					  // the current node.  The current node does not become the
					  // next node.
	void AddBeforeCurrent( void * );
					  // function that adds data <T> before the current node.
					  // The current node does not change.
	void AddAfterCurrent( void * );
					  // function that adds data <T> after the current node.
					  // The current node does not change.
	void AddHead( void * );
	void AddHead( XMLObject *pO) {AddHead((void *)pO);}
					  // function that add data <T> to the beginning of the list.
					  // The first node becomes the new node.
	void AddLast( void * );
	void AddLast( XMLObject *pO ) {AddLast((void *)pO);}
					  // function that add data <T> to the end of the list.
					  // The last node becomes the new node.
	bool isEmpty(void) const { return iSize == 0; };
					  // returns True if the list is empty, False otherwise
	int  Size(void) const { return iSize; };
	int  GetCount(void) { return iSize;}; // added for users most familiar with MFC list 
					  // returns the number of nodes in the list
	void RemoveAll(void);
					  // removes all nodes from the list, making an empty list
	
	int Remove(void *Data); // returns 1 if found and removed, otherwise Data was not in the list

	void *RemoveFirst(void);
					  // removes the first node from the list
	void *RemoveLast(void);
					  // removes the last node from the list
	void RemoveCurrent(void);
					  // removes the current node from the list
	void RemoveNext(void);
					  // removes the next node from the list,
					  // i.e.  the node following the current node
	void RemovePrevious(void);
					  // removes the previous from the list,
					  // i.e.  the node before the current node

	void DeferDestruction(); // defer ~dtor execution until Destruction()
	void Destruction();
};

class GListIterator
{
	GList::Node *iDataNode;    // used to return reference to data
	GList *pTList;
public:
	GList::Node *iCurrentNode;    // used to return reference to data

	GListIterator(const GList *iList, int IterateFirstToLast = 1);
		// List Iterator Constructor,
		// iList is a pointer to the list to be iterated
		// IterateFirstToLast is a boolean that indicates that the list
		// will be traversed from "First Node to Last Node" or
		// "Last Node to First Node".
		// If IterateFirstToLast == 1, use the operator++ to iterate list,
		// otherwise, use the operator-- to iterate list.
	~GListIterator() {};
	void reset(int IterateFirstToLast = 1);
	int operator () (void) const;
		// returns True if there are more node's to iterate in the list,
		// False otherwise.
	void * operator ++ (int); // post increment
		// returns a reference to the current node's data, advances the
		// current node to the next node.  Use operator! to test for end 
		// of iteration.
	void * operator -- (int); // post decrement
		// returns a reference to the current node's data, positions the
		// current node to the previous node.  
};

#endif // _GLIST_H____
