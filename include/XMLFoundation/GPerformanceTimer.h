// --------------------------------------------------------------------------
//						United Business Technologies
//			  Copyright (c) 2000 - 2009  All Rights Reserved.
//
// Source in this file is released to the public under the following license:
// --------------------------------------------------------------------------
// This toolkit may be used free of charge for any purpose including corporate
// and academic use.  For profit, and Non-Profit uses are permitted.
//
// This source code and any work derived from this source code must retain 
// this copyright at the top of each source file.
// --------------------------------------------------------------------------

#ifndef _PERFORMANCE_PROFILE_
#define _PERFORMANCE_PROFILE_
#ifndef WINCE

#include <sys/timeb.h>
#include <time.h>
#include <stdlib.h>


#ifndef _WIN32
	#define _timeb timeb
	#define _ftime ftime
#endif

inline long getTimeMS()
{
	struct _timeb tb;
	_ftime(&tb);
   	return tb.time * 1000 + tb.millitm;
}

#ifdef _WIN32
#	define __int64 long long
#	define PERF_START
#	define PERF_STOP
#else
#	define PERF_START						\
		{									\
			_asm mov eax, 0					\
			_asm cpuid						\
			_asm rdtsc						\
			_asm mov ebx, mrsb				\
			_asm mov dword ptr [ebx], eax   \
			_asm mov dword ptr [ebx+4], edx \
			_asm mov eax, 0					\
			_asm cpuid						\
		}
#	define PERF_STOP						\
		{									\
			_asm mov eax, 0					\
			_asm cpuid						\
			_asm rdtsc						\
			_asm mov ebx, mrse				\
			_asm mov dword ptr [ebx], eax	\
			_asm mov dword ptr [ebx+4], edx	\
			_asm mov eax, 0					\
			_asm cpuid						\
		}

#endif

#ifdef _WIN32
	__int64 I64MSRB, I64MSRE;
	void *mrsb = &I64MSRB;
	void *mrse = &I64MSRE;
#endif

class GPerformanceTimer
{	
	long     startTime;
	__int64 *m_plTime;
	int      m_bCout;
	int      m_bMillis;

public:

	GPerformanceTimer(__int64 *lTime = 0, const char *pzMessage = 0, int nMillis = 1) :
	    m_bMillis(nMillis),
		m_plTime(lTime)
	{
		m_bCout = 0;
		if (pzMessage)
		{
			printf("[");
			printf(pzMessage);
			printf("]=");
			m_bCout = 1;
		}

		if (m_bMillis)
		{
			startTime = getTimeMS();
		}
		else
		{
			PERF_START;
		}
	}
	
	// 1 for milliseconds, otherwise CPU cycles if available on your OS
	GPerformanceTimer(const char *pzMessage, int nMillis = 1) :
	    m_bMillis(nMillis)
	{
		m_plTime = 0;
		m_bCout = 0;
		if (pzMessage)
		{
			printf("[");
			printf(pzMessage);
			printf("]=");
			m_bCout = 1;
		}

		if (m_bMillis)
		{
			startTime = getTimeMS();
		}
		else
		{
#ifdef _WIN32
			PERF_START;
#else
			// No CPU Cycle count implemented for this OS, use milliseconds
			m_bMillis = 1;
			startTime = getTimeMS();
#endif
		}
	}

	~GPerformanceTimer()
	{
		PERF_STOP;
		if (m_bMillis)
		{
			if (m_plTime)
				*m_plTime = getTimeMS() - startTime;
			if (m_bCout)
			{
				printf("%d milliseconds \n",getTimeMS() - startTime);
			}
		}
		else
		{
#ifdef _WIN32
			if (m_plTime)
			{
				*m_plTime = I64MSRE-I64MSRB;
			}
			if (m_bCout)
			{
				char perfmtrbuf[100];
				_ui64toa(I64MSRE-I64MSRB, perfmtrbuf, 10);
				printf(perfmtrbuf);
				printf(" cycles \n");
			}
#endif
		}
	}
};


#endif // WINCE
#endif //_PERFORMANCE_PROFILE_
