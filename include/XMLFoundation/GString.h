// --------------------------------------------------------------------------
//						United Business Technologies
//			  Copyright (c) 2000 - 2009  All Rights Reserved.
//
// Source in this file is released to the public under the following license:
// --------------------------------------------------------------------------
// This toolkit may be used free of charge for any purpose including corporate
// and academic use.  For profit, and Non-Profit uses are permitted.
//
// This source code and any work derived from this source code must retain 
// this copyright at the top of each source file.
// --------------------------------------------------------------------------



//
//   GString.....
//
// 2X faster than using an ostream with a preallocated strstreambuf.
// 10X more portable.  You're covered if you have your GString on.
// It's more robust than CString - more rogue than RWString - and more std than std::string
//
// This class is actually a cross between a string and a stream.  It's a hybrid.
// It operates natively on both string data and binary content and it compiles everywhere.
//
//



#ifndef _G_STRING_HH_
#define _G_STRING_HH_



#ifndef ___max
#define ___max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef ___min
#define ___min(a,b)            (((a) < (b)) ? (a) : (b))
#endif


//#ifndef _WIN32
	#ifndef __int64
		#define	__int64 long long
	#endif
//#endif


#include <stdarg.h> // for va_list declaration


class GString
{
protected:
	char *_str;	// pointer to string data
	long  _len; // length of string data not counting terminating null
	long  _max; // current memory allocation size of _str
    unsigned short* _pWideStr; // Unicode working storage, type is (wchar_t *)
	void resize(); // grows _max and moves data from old memory to new larger contigious memory

	// allocated along with 'this' to reduces malloc/free calls.
	// When a GString < 256 bytes is constructed, _initialbuf is used.
	// As the string grows beyond 256, _strIsOnHeap becomes true and
	// _str no longer points into _initialbuf, but now is on the heap.
	char _initialbuf[256];
	int _strIsOnHeap; // 0 when we're working in our _initialbuf
					  // 1 when were working in memory we allocated OR own
					  // 2 when working in Attached() memory

public:
	// System wide GString settings that affect all GStrings on all threads
	static GString	g_FloatFmt;			// "%.7g"
	static GString	g_DoubleFmt;		// "%.7g"
	static GString	g_LongDoubleFmt;	// "%.7Lg"

	// frees the buffer
	virtual ~GString();

	// constructs an empty string
	GString(long nInitialSize = 256);

	// constructs a copy of the source string 
	GString(const GString &src);
	GString(const GString &src, int nSourceCount);

	// constructs a copy of the character string
	GString(const char *pzSource);
	GString(const char *pzSource, int nSourceCount);

	// constructs a string with ch repeated nCnt times
	// if you get a compile error "2 overloads have similar conversions"
	// while using this ctor cast nCnt to a short:   GString strXs('X',(short)777)
	GString(char ch, short nTimesToRepeat);


	// Attach to a memory buffer, WITHOUT making a copy of the memory.
	// ------------------------------------------------------------
	// constructs a string attached to supplied memory
	// so that the memory is NOT copied into this GString.
	// See notes for Attach(), they apply to this ctor as well
	// so to create a GString on the heap that uses memory on the stack:
	//
	//     char p[512]; strcpy(p,"hello world");
	//     GString *p2 = new GString(p,strlen("hello world"),512,0);
	//
	//   obviously, that stack frame must outlast the duration of p2, or it will crash violently.
	//		
	// This is the 'ctor version of:
	// void Attach(const char *pzSource, int nSourceBytes, long nSourceMemorySize, int nOwn);
	// Read Attach() comment immediately below - it all applies to this 'ctor as well
	GString(const char *pzSource, int nSourceBytes, long nSourceMemorySize, int nOwn);

	
	// Attach to a memory buffer, WITHOUT making a copy of the memory.
	// ------------------------------------------------------------
	// pzSource is a pointer to memory that you allocated
	// nSourceMemorySize is the allocated size of pzSource
	// nSourceBytes is the number of bytes currently in pzSource
	//
	// nOwn is true(1) or false(0) indicating if ther GString object now owns the memory.
	// if nOwn == true, then you alloc(new) pzSource, and the GString will free(delete) it
	// when when the scope of the object has been reached(in the destructor).
	//
	// if nOwn == false, you alloc/dealloc(new/delete) the buffer and you ensure
	// that pzSource is valid for the duration of this object. When the object goes out of scope
	// buf is NOT deleted by this object because you indicated GString does not own it.
	//
	// If any append operation(like << or +=) extends past the region of memory YOU allocated 
	// that is nSourceMemorySize bytes  - GString will allocate new memory and the attached 
	// memory will be deleted if nOwn = true otherwise (if nOwn = false) the buffer will be 
	// unattached from the GString and left for you to delete.
	//
	// note: although pzSource is const, it may be modified depending on what methods you call.
	// note: pzSource may contain nulls, binary or unprintable bytes.  nSourceBytes is the length and
	// GString never tries to strlen(pzSource) to determine the size.
	void Attach(const char *pzSource, int nSourceBytes, long nSourceMemorySize, int nOwn);


	// WriteOn() overwrites any existing value with n bytes from the src 
	// if src is not null terminated it will be terminated in the GString owned copy of it.
	// 
	// s = "one two three four five six "; s.WriteOn("seven");
	// now the value of s is only "seven"
	void WriteOn(const char *pSource,int nSourceBytes);
	void WriteOn(unsigned char *pSource,int nSourceBytes){WriteOn((const char *)pSource,nSourceBytes);};

	// write() appends to the GString to mimic ostream 
	//
	// s = "Just when you think you've seen it all"; s.write("\0there is more.",15);
	// now the Length() is the length of the entire string contents, therefore
	// s.Length() > strlen(s);
	void write(const char *pSource,int nBytes); // lower case for ostream interoperability just replace "ostream" with GString types and recompile for 2X string manipulation speed.
	void Write(const char *pSource,int nBytes){write(pSource,nBytes);}; // upper case for Development tools method sorting
	void write(unsigned char *pSource,int nBytes){write((const char *)pSource,nBytes);}; 
	void Write(unsigned char *pSource,int nBytes){write((const char *)pSource,nBytes);}; // upper case for Development tools method sorting

	
	// Load this GString from a file and replace any contents in this GString - returns 1 for success, 0 for fail
	int FromFile(const char* pzFileName, bool bThrowOnFail = 1);
	// Append the contents of the specified file to the end of this GString - returns 1 for success, 0 for fail
	int FromFileAppend(const char* pzFileName, bool bThrowOnFail = 1);
	// Save this GString to a file - overwrite if file exists
	int ToFile(const char* pzFileName, bool bThrowOnFail = 1);
	// Append this GString to a file - create file if not existing  returns 1 for success, 0 for failure
	int ToFileAppend(const char* pzFileName, bool bThrowOnFail = 1);


	
	//	The mask can contain any printable character and is terminated 
	//	by a null.
	//
	//	The character '_' in the mask will be replaced by a single character
	//	in the source string.  The character '*' will be replaced by the
	//	remaining characters in the source string.  Both '_' and '*' can be
	//	escaped using the character '\'.  You can escape the escape character
	//	with an escape character, i.e. "\\".
	//
	//	Some example masks:
	//
	//	Phone number mask: (___)___-____ [ext:*]
	//	SSN mask: ___-__-____	(tip: don't use SSN as a key in your database!)
	//
	//	If the source data is larger than the mask and the mask doesn't contain
	//	the wild character '*' the source data will be truncated.
	void MergeMask(const char *szSrc, const char *szMask);

	// FormatNumber formats the value of the GString using the format
	// string specified by szFormat
	void FormatNumber(const char *szFormat, 
					  char decimal_separator = '.',
					  char grouping_separator = ',',
					  char minus_sign = '-',
					  const char *NaN = "NaN",
					  char zero_digit = '0',
					  char digit = '#',
					  char pattern_separator = ';',
					  char percent = '%',
					  char permille = '?');

	//  Appends an ASCII/HEX dump to this string. Looks like this:
	//	2F 31 2E 30 0D 0A 41 63 63 65 70 74 3A 20 2A 2F 2A 0D 0A 41 63 63 65 70 74      /1.0..Accept: */*..Accept
	//	2D 4C 61 6E 67 75 61 67 65 3A 20 65 6E 2D 75 73 0D 0A 55 73 65 72 2D 41 67      -Language: en-us..User-Ag
	//
	// You may set bIncludeAscii=0, to remove the line breaks and readable column of text from the result.
	// note: this appends to the string so you may first prefix the dump with context information:
	// Common usage: s="recv'd:"; s.FormatBinary(buf,N); s.ToFileAppend("debug.txt")
	void FormatBinary(unsigned char *pData, int nBytes, int bIncludeAscii=1);
	void FormatBinary(const GString &strBinary, int bIncludeAscii =1);

	// CommaNumeric() - 123456789 will become 123,456,789
	// CommaNumeric() - 1000 will become 1,000
	// returns pointer to buffer inside 'this'
	const char *CommaNumeric();

	// convert 'this' to a user friendly byte notation with 2 digit unrounded percision
	// returns pointer to buffer inside 'this'
	//-----------------------------------------------------------------------------------
	// 999		=  999		1000000		=	1.0Mb	1000000000000	=1.0Tb
	// 1000		= 1.0Kb		10000000	=	 10Mb	9500000000000	=9.5Tb
	// 10000	=  10Kb		999999999	=	999Mb	999999999999999	=999Tb
	// 999999	= 999Kb		1000000000	=	1.0Gb
	const char *AbbreviateNumeric();


	// Call this member function to write formatted data to a 
	// GString in the same way that sprintf formats data into 
	// a C-style character array. 
	void Format(const char *lpszFormat, ...);// replaces 'this' value
	void FormatAppend(const char *lpszFormat, ...); // appends to 'this'
	void FormatPrepend(const char *lpszFormat, ...); // prepends to 'this'
	void FormatV(const char *lpszFormat, va_list argList);

	// removes leading and trailing quotation's from string
	void StripQuotes();

	// ------------  COMPARE ------------------------------
	// Returns zero if the strings are identical, 
	// < 0 if this GString object is less than pData, 
	// or > 0 if this GString object is greater than pData.
	//
	// so:
	// s = "MyFile.Exe";
	// s.Compare("YourFile.exe")		!=  0
	// s.Compare("myfile.exe")			!=  0
	// s.Compare("MyFile.exe")			==  0
	// s.CompareNoCase("myfile.exe")	==  0
	//
	// comapre pData 'till it's null
	int Compare(const char *pData) const; 
	// comapre pData 'for n bytes
	int Compare(const char *pData, int n) const; // comapre pData for n bytes
	int Compare(GString &rData) const;
	// same thing as above case insensitive
	int CompareNoCase(const char *pData) const;
	int CompareNoCase(const char *pData, int n) const;
	int CompareNoCase(const GString &) const;


	// compare a part of THIS's value to pData
	// nStartAtSelfIndex is the 0 based byte offest in this object that the comparison begins.
	// nSelfLen is the number of bytes to comapre, -1 compares up to the null in THIS
	// so
	// to compare the last 4 bytes of THIS to pData
	// assert(s.Length() > 4) 
	// s.CompareSubNoCase(".exe",s.Length() - 4) == 0
	// --------------------------------------------------
	int CompareSub(const char *pData, int nStartAtSelfIndex, int nSelfLen = -1) const; 
	int CompareSubNoCase(const char *pData, int nStartAtSelfIndex, int nSelfLen = -1) const; 
	int CompareToFile(const char *pzFileAndPath, int nMatchCase=1);// default is exact case match

	// returns the length of the string
	inline long Length() const { return _len; }
	inline long GetLength() const { return _len; } // for CString interoperability

	// a fast 'truncate' of the string length
	//
	//	 if (s.CompareSubNoCase(".exe",s.Length() - 4) == 0)// <--if the last 4 bytes are ".exe"
	//		s.SetLength( s.Length() - 4 );					// <--remove them from the string
	//
	//	  if (s.GetAt(s.Length()-1) == '\\' )				// <-- If the last byte of the path is a shash
	//	    s.SetLength(s.Length() - 1);					// <-- shorten the string by 1 byte
	inline void SetLength(int nLen) { if (nLen > -1 && nLen < _max) {_len = nLen; _str[_len] = 0;} } 

	// There is no Max length to a GString.  
	// This is the Maximum length that the string may grow until the next memory allocation
	// or said differently, it is the heap/stack allocation size of _str
	long GetMaxLength() const { return _max; }

	// Reset() will empty the GString, and release any heap allocation it has made.
	// Only in a rare situation would you want to Reset(), that would be when you no longer want the
	// contents of the GString, but you are not going to destroy the GString object (suppose you are pooling GString Objects)
	// Empty() has the same effect, but does not release the buffers, so if you want to set a GString
	// to 0 length that will keep being reused - Empty() it so that it does not need to make new heap 
	// allocations next time you grow beyond 256 bytes.
	void Reset();
	
	// SetPreAlloc() sets the _max length obtained by GetMaxLength(), the contents of the string 
	// are unchanged and memory is pre-allocated for the amount of bytes specified.  The amount
	// specified may only grow the allocation - not reduce it, if you specify a value less than
	// current GetMaxLength() in your call to SetPreAlloc() the call is ignored.
	// SetPreAlloc() is the same as the "GString(nInitialSize)" 'ctor except that it works on pre-constructed GStrings
	void SetPreAlloc(long);
	// same as above, but it sets the additional allocated space desired
	void GrowPreAlloc(long l){SetPreAlloc(_len+l);};


	// escapes <>&"\ in 'this' with &#ascii-number;
	void EscapeXMLReserved();
	// appends pzSource onto 'this', escaping <>&"\ with &#ascii;
	// nLen = # bytes to append from pzSource or -1 to append until a null.
	void AppendEscapeXMLReserved(const char *pzSource, int nLen = -1);
	void AppendXMLTabs( int nTabs );


	// returns a character pointer to the string�s data.
	inline operator const char *() const { return _str; }
	

	// same as a (const char *) cast above - easier to use through a pointer ->
	// turns this [ (const char *)(*myStr) ] into [ myStr->StrVal() ]
	inline const char *StrVal() const { return _str; }

	// given s == "10.9.8.7.7.7." 
	// s.StartingAt(7) == "7.7.7"
	// The string is not trimmed, just indexed. s still == "10.9.8.7.7.7"
	// same as Buf() with error checking and const return
	const char *StartingAt( int n0BasedIndex ) {return (n0BasedIndex < _len) ? &_str[n0BasedIndex] : 0;}

	// Get the 'non const' buffer for this GString, otherwise the same as GString::StartingAt()
	// You should use GString::StartingAt() unless you want the results in non-const form.
	//
	// - if you modify the results
	//     1. MAKE SURE YOU DO NOT OVERWRITE THE BOUNDS 
	//     2. MAKE SURE YOU SET THE LENGTH AFTER MODIFYING THE STRING
	//     3. Don't index beyond  GetLength()
	//     Example:
	//     Assert(S.GetMaxLength() > strlen("Hello World"))
	//     strcpy(S.Buf(),"Hello World");
	//     S.SetLength(strlen("Hello World"));
	// - you may also want to use Buf() to avoid type casting off the 'const' for arguments to other function calls.
	char *Buf(int nIndex = 0) { return &_str[nIndex]; }


	// returns true if the string is empty
	bool IsEmpty() const { return _len == 0; }

	// Makes this GString object an empty string.
	void Empty() { _len = 0; _str[_len] = 0; }

	// Load an error from the active ErrorProfile() owned by GException.cpp
	void LoadResource(const char* szSystem, int error, ...);

	// overloaded subscript ([]) operator returns a single character 
	// specified by the zero-based index in nIndex. 
	char& operator[](int nIdx);
	char GetAt(int nIdx) const;
	// The SetAt member function overwrites a single character 
	// specified by an zero based index number. 
	void SetAt(int nIdx, char ch);

	// Pads the string object on the left with ch to make the string
	// a minimum of nCnt character's in length.  s="123" s.PadLeft(5) s = "00123"
	void PadLeft(int nCnt, char ch = ' ');
	// Pads the string on the left with nCnt of ch
	void Prepend(int nCnt, char ch = ' ');
	// Removes the character ch from the left side of the string.
	void TrimLeft(char ch = ' ', short nCnt = -1);
	// trim leading white space (tabs, CR, LF, spaces) from the left(beginning) of the string
	void TrimLeftWS(); 
	// removes nCnt bytes from the beginning of the string
	void TrimLeftBytes(int nCnt);
	// Pads the string object on the right with ch to make the string
	// a minimum of n character's in length  s="123" s.PadRight(5) s = "12300"
	void PadRight(int nCnt, char ch = ' ');
	// Pads the string on the right with nCnt of ch
	void Append(int nCnt, char ch = ' ');
	// Removes the character ch from the right side of the string. (max nCnt)
	void TrimRight(char ch = ' ', short nCnt = -1);
	// Shortens the sting by nCnt bytes
	void TrimRightBytes(int nCnt);
	// trim White Space from the end of the string 
	// removes trailing tabs, spaces, carriage returns, new lines
	void TrimRightWS(); 

	// Extracts the first (that is, leftmost) nCount characters from 
	// this GString object and returns a copy of the extracted substring. 
	// If nCount exceeds the string length, then the entire string is extracted. 
	GString Left (int nCnt) const;
	// Extracts a substring of length nCount characters from this GString object, 
	// starting at position nFirst (zero-based). 
	GString Mid  (int nFirst) const;
	GString Mid  (int nFirst, int nCnt) const;
	// Extracts the last (that is, rightmost) nCount characters from this GString 
	// object and returns a copy of the extracted substring. If nCount exceeds the
	// string length, then the entire string is extracted. 
	GString Right(int nCnt) const;

	// Converts this GString object to an uppercase string.
	// Converts entire string by default or specify begin index
	void MakeUpper(int nBeginIndex=0);
	// Converts this GString object to a lowercase string.
	void MakeLower(int nBeginIndex=0);
	// Reverses the characters in the GString
	void MakeReverse();

	// search the string starting at 0 based index nStart
	// return -1 if not found otherwise a 0 based index of the found data
	int FindCaseInsensitive( const char * lpszSub, int nStart = 0 ) const; 
	int Find( const char * pstr, int nStart = 0 ) const;
	int Find( char ch, int nStart = 0 ) const;
	
	// return -1 when Nth occurance is not found, otherwise index to the Nth occurance of pstr
	// Nth is a 1 based index, so Nth=1 is the first occurance, Nth=2 is the second, and so on.
	// Note: if you needed to split folder paths from Folder/Paths/To/File.txt
	// Consider using a GStringList like this: GStringList l("/","Folder/Paths/To/File.txt")
	// FindNth() is better suited for something that is always (before/at/after) the Nth occurance
	int FindNth( const char *pstr, int Nth, int nStart = 0 ) const;
	int FindNth( char ch, int Nth, int nStart = 0 ) const;

	// return -1 if none of the [pzCharsToSearchFor] were found in 'this' 
	// otherwise returns the index of the first char in 'this' that is in the [pzCharsToSearchFor]
	int FindOneOf(const char *pzCharsToSearchFor) const;

	// return a pointer into 'this' after the matched find
	// s="one=hello"     s2=FindStringAfter("one=")   s2 == "hello"
	// same as Find() but returns data rather than an index, and advances it past the match token - a common task
	const char *FindStringAfter(const char *pSearchFor) const;

	// a dynamic SubStr()
	// s="aaaBBBccc"    s2 = s.FindStringBetween("aaa","ccc")     s2 == "bbb"
	GString FindStringBetween(const char *pSearchForBegin, const char *pSearchForEnd) const;

	// All of the other Find* methods assume that this's string content is a null terminated string
	// This find searches this's contents for a binary match to the data in [strToFind] that may also be binary
	// nulls in either this or [strToFind] will not terminate the search.  Like the other finds, this returns 
	// the index that [strToFind] begins in [this] or -1 if it was not found, like the other find*'s [nStart]
	// is the 0 based index in [this] that the search begins at.
	int FindBinary(GString &strToFind, int nStart = 0);


	// search starting from the end of the string, 
	// nStart is a 0 based index of the starting place to search, by default nStart is _len (the last byte of the string)
	// the search just works toward the front - index 0.
	// if bMatchCase = 1, the ONLY EXacT Case matches will be found
	// returns -1, if [ToFind] is not found otherwise a 0 based index where it was found
	// 
	// example to obtain the extension from a file name if it has one
	// int nDot = strFile.ReverseFind(".");
	// GString strExtension( (nDot == -1) ? "" : strFile.StartingAt( nDot + 1 ) );
	int ReverseFind( char chToFind ) const;
	int ReverseFind( char chToFind, int nStart ) const;
	int ReverseFind( const char *pzToFind, int nStart = -1, int bMatchCase = 0 ) const;
	int ReverseFindNth( const char *pzToFind, int Nth) const;
	
	// starting from the end of the string, return the index of the first char in this that matches any char in pzToFind
	// s = "/usr/bin/file.ext" || s = "c:\the\path\file.ext"
	// s.SetLength(s.ReverseFindOneOf("/\\")); // cut off the file name for either a windows or unix path
	int ReverseFindOneOf( const char *pzToFind ) const;
	int ReverseFindOneOf( const char *pzToFind, int nStart  ) const;

	// assign the value of 'this' from pFrom upto the first match of pzUpToTerm in pFrom
	// example to get only the first 'line' from pFrom into 'this':   str.SetFromUpTo(pFrom,"\r\n");
	// returns index into pFrom, where first of pzTerm was found or -1 if no termination was found
	// s.SetFromUpto("one|two","|");
	// s == "one"
	//
	// strURL = "http://www.abc.com/path/file.txt"
	// The 7th byte follows "http://" in the URL. The IP or server name is an unknown length, everything upto the first slash (or pipe)
	// strDomain.SetFromUpToFirstOf( strURL.StartingAt(nHTTPIndex + 7), "\\/|" );  
	// strDomain == www.abc.com
	int SetFromUpTo(const char *pFrom, const char *pzUpToTerm);
	int AppendFromUpTo(const char *pFrom, const char *pzUpToTerm); // appends to 'this'
	// same as above but pFrom terminates at the first occurance of any character found in pzUpToTerm
	int SetFromUpToFirstOf(const char *pFrom, const char *pzUpToTerm);
	int AppendFromUpToFirstOf(const char *pFrom, const char *pzUpToTerm);


	// insert the the given value into 'this' at the given index.
	// s = "Rold"  s.Insert(2,"ck the Wor")  s == "Rock the World"
	// if you know the length of [str] or if it contains nulls as data pass the length of it as [nStrLen] otherwise [str] will be inserted up to the first NULL
	void Insert( int nIndex, char ch );
	void Insert( int nIndex, const char *str, int nStrLen = -1 );
	
	// search 'this' string for [pzMatch], and insert [pzInsertThis] before [pzMatch] for InsertBefore() or after [pzMatch] for InsertAfter()
	// if bMatchCase is set to 1, then ONLY ExAcT case matches for [pzMatch] will be used.
	// returns the index that [pzInsertThis] was inserted or -1 if [pzMatch] was not found.
	int InsertBefore( const char *pzMatch, const char *pzInsertThis, int bMatchCase = 0);
	int InsertAfter( const char *pzMatch, const char *pzInsertThis, int bMatchCase = 0);

	// remove [nLen] bytes starting at index [nStart]
	void Remove ( int nStart, int nLen );
	// remove all occurances of [ch], return count removed
	int RemoveAll ( char ch );
	// remove all occurances of [pStrToRemove], if [bMatchCase] is 1, then only EXaCT case matched are removed.  returns count removed.
	int RemoveAll ( const char *pStrToRemove, int bMatchCase = 0 );
	// remove first occurance of [ch], return 1 if removed otherwise 0
	int RemoveFirst ( char ch );
	// remove first occurance of [pStrToRemove], if [bMatchCase] is 1, then only EXaCT case matched are removed. return 1 if removed otherwise 0
	int RemoveFirst ( const char *pStrToRemove, int bMatchCase = 0 );
	// remove last occurance of [pStrToRemove], if [bMatchCase] is 1, then only EXaCT case matched are removed,  return 1 if removed otherwise 0
	int RemoveLast ( const char *pStrToRemove, int bMatchCase = 0 );
	// remove last occurance of [ch], return 1 if removed otherwise 0
	int RemoveLast ( char ch );


	// Replace ALL occurances of *What with *ReplaceWith, unless nFirstOccuranceOnly = 1, then only replace the first occurance
	void Replace( char chWhat, char chReplaceWith, int nFirstOccuranceOnly = 0 );
	void Replace( const char * szWhat, char chReplaceWith, int nFirstOccuranceOnly = 0  );
	void Replace( const char * szWhat, const char *szReplaceWith, int nFirstOccuranceOnly = 0 );
	void ReplaceCaseInsensitive( const char * szWhat, const char *szReplaceWith, int nStart = 0, int nFirstOccuranceOnly = 0 );
	void Replace( char chWhat, const char *szReplaceWith, int nFirstOccuranceOnly = 0 );

	// replace all occurances of each character in [pzCharSet] with [chReplaceWith]
	// s = "717273"
	// s.ReplaceChars("123",'_')
	// s == "7_7_7_"
	void ReplaceChars(const char *pzCharSet, char chReplaceWith);


	// overwrite the value of 'this' with pzNew beginning at nIndex
	// if nLength is -1, the entire pzNew will be written into 'this'
	// otherwise only nLength bytes from pzNew will be written.
	void OverWrite(const char *pzNew, int nIndex = 0, int nLength = -1);

	// replace each char in pzReplaceChars with %nn where
	// nn is a two byte hex value of the byte that was replaced.
	// example: GString s("A\nC");  
	//			s.EscapeWithHex("\r\n\t");
	//			s == "A%0AC"
	//
	const char *EscapeWithHex(const char *pzEscapeChars);
	// replace each %nn hex code with the ascii character it represents
	// s="A%0AC"    s.UnEscapeHexed()    s == "A\nC"
	void UnEscapeHexed();

	// x-path
	void NormalizeSpace();
	void Translate(const char *szConvert, const char *szTo);
	
	// unlike the append operators(+= and <<), that expand packed numeric to alpha nuneric, these
	// leave the packed numeric in binary form stored in network byte order.  If you are writing an
	// application that croses BigEndian and LittleEndian platforms, this will ensure proper binary conversion.
	// example:
	// __int64 n = 18446744073709551615
	// s1 << n;								// length of s1 is 20
	// s2.AppendPackedInteger(n);			// length of s2 is 8		  (packed with an intel or sparc processor)
	// __int64 n2 = s2.GetPacked64(0);		// n2 == 18446744073709551615 (unpacked with intel or sparc processor)
	void AppendPackedInteger(unsigned short);
	void AppendPackedInteger(unsigned long);
	void AppendPackedInteger(unsigned __int64);
	unsigned short GetPackedShort(int index);  // index into 'this' GString that the PackedShort is stored
	unsigned long GetPackedLong(int index);
	unsigned __int64 GetPacked64(int index);


	// if the GString value is numeric, change it's value.  For example
	// s = "700"; s.Inc(77); s == "777"
	void Inc(int nAmount = 1);				// Increment
	void Dec(int nAmount = 1);				// Decrement
	// same as above with an ASCII value. For example
	// s = "700"; s.Inc("77"); s == "777"
	void Inc(const char *pzAmount);			
	void Dec(const char *pzAmount);


	// assignment operators
	GString & operator=(char);
	GString & operator=(unsigned __int64);
	GString & operator=(__int64);
	GString & operator=(const char *);
	GString & operator=(const signed char *);
	GString & operator=(unsigned char);
	GString & operator=(signed char);
	GString & operator=(short);
	GString & operator=(unsigned short);
	GString & operator=(int);
	GString & operator=(unsigned int);
	GString & operator=(long);
	GString & operator=(unsigned long);
	GString & operator=(float);
	GString & operator=(double);
	GString & operator=(long double);
	GString & operator=(const GString &);

	// append operators, enables a GString to mimic an ostream for simple porting to faster GStrings
	GString & operator<<(unsigned __int64);
	GString & operator<<(__int64);
	GString & operator<<(const char *);
	GString & operator<<(const signed char *);
	GString & operator<<(char);
	GString & operator<<(unsigned char);
	GString & operator<<(signed char);
	GString & operator<<(short);
	GString & operator<<(unsigned short);
	GString & operator<<(int);
	GString & operator<<(unsigned int);
	GString & operator<<(long);
	GString & operator<<(unsigned long);
	GString & operator<<(float);
	GString & operator<<(double);
	GString & operator<<(long double);
	GString & operator<<(const GString &);

	GString & operator+=(unsigned __int64);
	GString & operator+=(__int64);
	GString & operator+=(const signed char *);
	GString & operator+=(const char *);
	GString & operator+=(char);
	GString & operator+=(unsigned char);
	GString & operator+=(signed char);
	GString & operator+=(short);
	GString & operator+=(unsigned short);
	GString & operator+=(int);
	GString & operator+=(unsigned int);
	GString & operator+=(long);
	GString & operator+=(unsigned long);
	GString & operator+=(float);
	GString & operator+=(double);
	GString & operator+=(long double);
	GString & operator+=(const GString &);

	// friend functions for addition
	friend GString operator+(GString &, GString &);
	friend GString operator+(GString &, const char *);
	friend GString operator+(const char *, GString &);
	friend GString operator+(GString &, const signed char *);
	friend GString operator+(const signed char *, GString &);
	friend GString operator+(GString &, char);
	friend GString operator+(char, GString &);
	friend GString operator+(GString &, unsigned char);
	friend GString operator+(unsigned char, GString &);
	friend GString operator+(GString &, signed char);
	friend GString operator+(signed char, GString &);

	// Comparitive operators with other GString classes
	int operator >  (const GString &) const;
	int operator >= (const GString &) const;
	int operator == (const GString &) const;
	int operator <  (const GString &) const;
	int operator <= (const GString &) const;
	int operator != (const GString &) const;

	// Comparitive operators with char *'s
	int operator >  (const char *) const;
	int operator >= (const char *) const;
	int operator == (const char *) const;
	int operator <  (const char *) const;
	int operator <= (const char *) const;
	int operator != (const char *) const;

#ifdef _UNICODE
	GString & operator<<(const wchar_t *);
	GString & operator+=(const wchar_t *);
	operator wchar_t * () const;
	wchar_t *Unicode();
#endif
};



char * stristr(const char * str1, const char * str2); // in GString.cpp
__int64 Xatoi64(const char* s);	// in GString.cpp

// Gstrlen is a strlen that takes char * or unicode strings
unsigned int Gstrlen( const char *str );
#ifdef _UNICODE
 unsigned int Gstrlen( const wchar_t *str );
 wchar_t *DLAsciiToUnicode(const char *a, char *b);
#endif


#undef NEED_ICOMP

#ifdef WINCE
 #define NEED_ICOMP
#endif
#ifndef _WIN32
 #define NEED_ICOMP
#endif

#ifdef NEED_ICOMP
	int stricmp( const char *string1, const char *string2 );
	int strnicmp(const char *p1, const char *p2, unsigned int nMax);
#else
	#define strcasecmp	stricmp
#endif






/*	The following comments only apply when using a GString for COM programming.

	NOTE 1:	The following code is for COM programming, it is intentionally NOT included
			in the GString class so that GString has no dependencies on COM datatypes or Windows.

	NOTE 2:	When building ATL com objects that use GString::ToFile*()
			you must remove _ATL_MIN_CRT from the preprocessor directives.  Unless you call ToFile(), not an issue.

	NOTE 3:	You can #include "GStringCOM.h" for an inlined string implementation that can convert to and from BSTR
			or you can use the standard GString class along with these functions that you must copy
			into your application so that the XMLfoundation does not have any dependancies on COM
			datatypes or Windows kernel translation routines.

			void BSTR2GString(GString &pStr, BSTR bstr)
			{
				int nLen = SysStringLen(bstr);
				int nBytes = WideCharToMultiByte(CP_ACP, 0, bstr, nLen, NULL, NULL, NULL,NULL);
				pStr.SetPreAlloc(nBytes + 1);
				LPSTR lpsz = pStr.Buf();
				WideCharToMultiByte(CP_ACP, 0, bstr, nLen, lpsz, nBytes, NULL, NULL);
				pStr.SetLength(nBytes);
			}

			void GString2BSTR(GString &pStr, BSTR FAR* bstr)
			{
				int nStrLen = pStr.GetLength();
				int nLen = MultiByteToWideChar(CP_ACP, 0, pStr.Buf(),	nStrLen, NULL, NULL);
				::SysReAllocStringLen(bstr,NULL,nStrLen);
				MultiByteToWideChar(CP_ACP, 0, pStr.Buf(), nStrLen,*bstr, nLen);
			}
*/

#endif //_G_STRING_HH_

