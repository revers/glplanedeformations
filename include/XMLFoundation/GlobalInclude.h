//
// Common Global Include File
//

#ifndef new
#define NEWER_NEW new(SOURCE_FILE, __LINE__, 'X')
void * operator new( unsigned int n, const char *pzFile, int nLine, char chUniqueOverload);
void operator delete( void* p, const char *pzFile, int nLine, char chUniqueOverload );
#endif

// Uncomment this for the overloaded new/delete in memory.cpp to be called.
// After you uncomment it, you need to rebuild XMLFoundation, 
// AND your application AND any other libs that you use like Xfer.lib or XMLServer.lib
//
// #define new NEWER_NEW



