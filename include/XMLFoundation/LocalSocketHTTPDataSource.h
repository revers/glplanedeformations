#ifndef _SOCKET_LOCAL_HTTP_DS_INC__
#define _SOCKET_LOCAL_HTTP_DS_INC__


#include "SocketHTTPDataSource.h"

#include "SQLExecution.h"
#include "DataAbstraction.h"
#include "xmlServer.h"


class CFileDataSource;
class GString;
class CLocalSocketHTTPDataSource : public CSocketHTTPDataSource
{
	GString *m_pzLocalXML;
	CFileDataSource *m_pFileDataSource;

public:
	CLocalSocketHTTPDataSource(const char **ppQueryAttributes = 0);

	const char *send(const char *pzProcedureName, 
				  const char *xml, 
				  int nXMLLen, 
				  void *PooledConnection,
				  void **ppUserData,const char *pzNameSpace = "TransactXML=");

	int release(const char *resultFromSend, void *PooledConnection,void *ppUserData);

	void SetFileDataSource(CFileDataSource *pFileDataSource)
	{
		m_pFileDataSource = pFileDataSource;
	}
};


#endif //_SOCKET_LOCAL_HTTP_DS_INC__