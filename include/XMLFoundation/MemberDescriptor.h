// --------------------------------------------------------------------------
//						United Business Technologies
//			  Copyright (c) 2000 - 2009  All Rights Reserved.
//
// Source in this file is released to the public under the following license:
// --------------------------------------------------------------------------
// This toolkit may be used free of charge for any purpose including corporate
// and academic use.  For profit, and Non-Profit uses are permitted.
//
// This source code and any work derived from this source code must retain 
// this copyright at the top of each source file.
// --------------------------------------------------------------------------

#ifndef _MEMBER_MAP_ENTRY_H
#define _MEMBER_MAP_ENTRY_H

#ifndef _WIN32
	#ifndef __int64
		#define	__int64 long long
	#endif
#endif


#include "AttributeList.h"
#include "xmlDefines.h"
#include "ListAbstraction.h"

class StackFrameCheck;
class MemberHandler;
class StringAbstraction;
class XMLAttributeList;
class XMLObject;

class MemberDescriptor
{
	// a bit flag field containing NULL, DIRTY, CACHE states
	int m_memberStateFlags;

	// user defined data structure
	void *m_pUserData;

public: 
	// pointer to object that contains the member described by this structure
	XMLObject *m_ParentObject;
	
	// almost always true.  See XMLObject::IsFirstMap() in XMLObject.cpp
	int m_bFirstMap;

	void *GetItemData();
	void  SetItemData(void *);

	~MemberDescriptor();
	void Init( XMLObject *pParent );
	void GetTypeName(GString &strTypeDescription);
	bool GetMemberValue(GString &strValue);

	// Construct a Root element used to store tag-to-objectFactory mappings for the Factory Manager
	MemberDescriptor(XMLObject *pParent, const char *pzTag, ObjectFactory pFactory, const char * pzClassType);

	// This contructs the MemberDescriptor to refer to an Entity.  It causes the 
	// Entity to be serialized out by name, so that the Object can restore by the
	// value that the tokenizer will put in it's place.
	MemberDescriptor(XMLObject *pParent, const char *pzEntityName);
        
        // Revers:
	MemberDescriptor(XMLObject *pParent, const char *pzTag, float *pVal);
        MemberDescriptor(XMLObject *pParent, const char *pzTag, double *pVal);

	// Construct an integer XMLEntry
	MemberDescriptor(XMLObject *pParent, const char *pzTag, int *pVal);
	MemberDescriptor(XMLObject *pParent, const char *pzTag, long *pVal);
	MemberDescriptor(XMLObject *pParent, const char *pzTag, __int64 *pVal);
	// Construct a string XMLEntry
	MemberDescriptor(XMLObject *pParent, const char *pzTag, void *pVal, StringAbstraction *pHandler);
	// Construct a sub-Object pointer XMLEntry
	MemberDescriptor(XMLObject *pParent, const char *pzTag, XMLObject **pObj, ObjectFactory pFactory );
	// Construct a sub-Object XMLEntry
	MemberDescriptor(XMLObject *pParent, const char *pzTag, XMLObject *pObj);
	// Construct an Object Collection XMLEntry
	MemberDescriptor(XMLObject *pParent, const char *pzTag, void *pList, ListAbstraction *pHandler, ObjectFactory pFactory);
	// Construct a member that will be controlled by supplied handler object
	MemberDescriptor(XMLObject *pParent, const char *pzTag, MemberHandler *pHandler);
	// Construct a string collection mapping entry
	MemberDescriptor(XMLObject *pParent, const char *pzTag, void *pStringCollection, StringCollectionAbstraction *pHandler);
	// Construct an integer array mapping entry
	MemberDescriptor(XMLObject *pParent, const char *pzTag, void *pIntegerArray, IntegerArrayAbstraction *pHandler);
	// Construct a member structure that contains object/key pairs (binary tree, hash etc)
	MemberDescriptor(XMLObject *pParent, const char *pzTag, void *pKeyedDSA,KeyedDataStructureAbstraction *pHandler);

	// tree nodes alphebetically sorted by Tag
	MemberDescriptor *Left;    
	MemberDescriptor *Right;

	// This is the Key for searching the tree, supplied by the derived class.
	// This is the tag that the XML Parser uses to obtain storage for element data
	// when it encounters a new tag in the XML.
	GString strTagName;

	// Always NULL unless pointing to member sub-object of type Collection.
	ObjectFactory m_pfnFactoryCreate; 

	// XML serialization support for the member that this entry describes
	void MemberToXML(GString& xml, int nTabs, StackFrameCheck *pStack,int nSerializeFlags);
	static void TabifyXML(GString& xml, int nTabs, int nFlags =0 );

	// Object's attributes are stored in the object, not in this descriptor
	// so if IsSubObject() is true, m_pAttributes will always be null. Most 
	// member variables have no attributes, so only when a member with 
	// attributes is encountered is m_pAttributes allocated. 
	XMLAttributeList *m_pAttributes;

	// Access to attributes of the member.
	void AddAttribute( const char * pzName, const char * pzValue );
	void AddAttribute( const char * pzName, int nValue );
	const char *FindAttribute( const char *pzAttName );
	
	void SetAsTempObject(XMLObject *pO);


	// Native, and user defined datatype support
	enum DataTypes 
	{ 
		XMLObj, 
		XMLObjPtr,
		UserString,
		UserStringCollection,
		UserIntegerArray,
		XMLObjList,
		Int,
		Long,
		Int64, 
		Str, 
		Root, 
		Bool, 
		ManagedByDerivedClass,
		KeyedDataStructure,
		EntityReference,
                // Revers:
                Float,
                Double         
	} DataType;

	// This member may represent 0-n objects. These methods provide 
	// information about, and access to the Contained Objects.
	bool IsSubObject();
	bool IsSimpleType();

	
	// If this member contains 1 or more objects, the following set
	// of methods abstract a common interface no matter what the 
	// specific type of object container is.
	int AddContainedObject( XMLObject *pO );
	int  GetObjectContainmentCount();
	void RemoveLastContainedObject( xmlObjectIterator iter, GString &strtag, XMLObject *pAddressOfInvalidMemory );
	XMLObject *GetNextContainedObject( xmlObjectIterator iter );
	XMLObject *GetFirstContainedObject( xmlObjectIterator *iter );
	XMLObject *GetKeyedContainedObject( const char *key );
	void ReleaseObjectIterator( xmlObjectIterator iter );

	// if this abstracted type supports deferred destruction (GList, GHash) - then cleanup
	void DeferredDestruction();


	// When the XML Parser sets the data, it is setting the initial value
	// so it is not considered dirty, however when an application developer
	// sets the state of a member variable, it is now dirty.
	void Set(const char * pzData, int nDataLen, bool bSetDirty, bool bUpdateCleanMembers);
	// same, uses "member set flags" in xmlDefines.h
	void Set(const char * pzData, int nDataLen, unsigned int nSetFlags);


	bool IsDirty();
	bool IsNull();
	bool IsCached();
	bool IsSerializable();

	void SetSerializable( bool bSerialize );
	void SetDirty( int bDirty = 1 );

	// These are mutually exclusive and store the location of 
	// XMLObject derived member data.  
	// If the derived type is a collection, pObjectList contains 
	//     storage for objects created from the m_pfnFactoryCreate.
	// pObject points to a member sub-Object in the derived class.
	union DERIVED_DATA_STORAGE
	{
		const char *pClassName;
		char **ppNativeString;
		bool *pBool;
		XMLObject *pObject;
		XMLObject **ppObject;
		void *pUserString;
		void *pXMLObjList;
		void *pUserStringCollection;
		void *pUserIntegerArray;
		int *pInt;
		long *pLong;
		void *pKeyedDSA;
		__int64 *pInt64;
                float *pFloat;
                double *pDouble;
	}m_Member;  

	// abstration handlers are used make atomic operations.
	union DERIVED_DATA_MANAGEMENT_ABSTRACTION
	{
		ListAbstraction *pListHandler;
		StringAbstraction *pStringHandler;
		MemberHandler *pMemberHandler;
		StringCollectionAbstraction *pStrListHandler;
		IntegerArrayAbstraction *pIntArrayHandler;
		KeyedDataStructureAbstraction *pKeyDSA;
	}m_DataAbstractor;
};

#endif //_MEMBER_MAP_ENTRY_H
