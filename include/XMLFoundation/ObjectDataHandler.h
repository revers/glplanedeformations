// --------------------------------------------------------------------------
//						United Business Technologies
//			  Copyright (c) 2000 - 2009  All Rights Reserved.
//
// Source in this file is released to the public under the following license:
// --------------------------------------------------------------------------
// This toolkit may be used free of charge for any purpose including corporate
// and academic use.  For profit, and Non-Profit uses are permitted.
//
// This source code and any work derived from this source code must retain 
// this copyright at the top of each source file.
// --------------------------------------------------------------------------



// This allows an object to have a value that is not in a member variable.
// The concept is not allowed in C++, but is valid XML syntax.  For example,
// <SomeObject>										// maps to object
//		This is the object value					// maps to ObjectDataHandler
//		<ObjectMember>member value</ObjectMember>	// maps to object member
// <//SomeObject>
// if an object is interested in "subscribing" to this data that does not 
// have a cooresponding member variable the following methods can be overriden.

// A real world usage example :
// <PhoneNumber>
// 	   (415)123-4567x123
// </PhoneNumber>
// where the object model defines PhoneNumber as an object, not a string.
//
// class PhoneNumber : public XMLObject, public XMLObjectDataHandler
// {
// 	string areacode;
// 	string prefix;
// 	string suffix;
// 	string extension;
//  MapXMLTagsToMembers(){/*nothing to map*/}
//	PhoneNumber()
//	{
//		Register the special data handler as 'this' object, You may use
//		any object derived from XMLObjectDataHandler and eliminate the
//		multiple inheritance used in this example.
//		setObjectDataHandler(this);
//	}
//	SetObjectValue(const char *strSource)
//	{
		// parse the data "\n\t(415)123-4567x123\n\0"
		// and place the values in the cooresponding
		// member variables.
//	}

//	You must escape "&'<> with &quot; &amp; &apos; &lt; and &gt;
//  you can use GString::AppendEscapeXMLReserved() to achieve this
//	AppendObjectValue(GString& xml)
//	{
//		xml << "(" << xml.AppendEscapeXMLReserved(areacode) << ")"
//			<< xml.AppendEscapeXMLReserved(prefix) << "-"
//			<< xml.AppendEscapeXMLReserved(suffix) << "x"
//			<< xml.AppendEscapeXMLReserved(extension);
//	}
// };

#ifndef _XML_OBJECT_HANDLER_H__
#define _XML_OBJECT_HANDLER_H__

class XMLObjectDataHandler
{
public:
	// called by the framework to assign the value to your object
	virtual void SetObjectValue(const char *strSource) = 0;
	// called by the framework during serialization for you 
	// to add the object value to the XML stream
	virtual void AppendObjectValue(GString& xml) = 0;
};

#endif //_XML_OBJECT_HANDLER_H__

