//***************************************************************************
// Copyright (c) 1998,1999,2000,2001 United Business Technologies, Inc.
// All Rights Reserved.  Source in this file is released to the public.
// under the following conditions:

// The Source is Owned and maintained by UBT, this is our C++ client side
// toolkit for working with incomming xml messages.  This toolkit is 
// free standing accepting input from disk or memory and may be used free
// of charge for any purpose including corporate and academic use.

// UBT welcomes any suggestions, improvements or new platform ports that
// developers wish to 'donate' to the general build maintained by UBT.  

// This Notice must be kept on all variations and additions to this source

//					www.UnitedBusinessTechnologies.com
//***************************************************************************
#if !defined(AFX_PASSWORDHELPER_H__012B86DC_B981_11D3_A6EA_00A0CC542CEF__INCLUDED_)
#define AFX_PASSWORDHELPER_H__012B86DC_B981_11D3_A6EA_00A0CC542CEF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class PasswordHelper  
{
public:
	static void Hash(const char* pszSalt, const char* pszPass, unsigned char digest[16]); 

};

#endif // !defined(AFX_PASSWORDHELPER_H__012B86DC_B981_11D3_A6EA_00A0CC542CEF__INCLUDED_)
