// --------------------------------------------------------------------------
//						United Business Technologies
//			  Copyright (c) 2000 - 2009  All Rights Reserved.
//
// Source in this file is released to the public under the following license:
// --------------------------------------------------------------------------
// This toolkit may be used free of charge for any purpose including corporate
// and academic use.  For profit, and Non-Profit uses are permitted.
//
// This source code and any work derived from this source code must retain 
// this copyright at the top of each source file.
// --------------------------------------------------------------------------
#include "BaseInterface.h"


typedef void (*DriverExec)(void *pCBArg, const char *pzCmd);
typedef void (*CBfn)(const char *, void *);

class PlugInController
{
	DriverExec m_Exec;
	CBDataStruct *m_pcb;
	char m_StringResultBuffer[64];
public:

	PlugInController(void *pHandle, DriverExec Exec)
	{
		m_Exec = Exec;
		m_pcb = (CBDataStruct *)pHandle;
	}
	void SetProcessInstruction(const char *pzPI)
	{
		// add the Process Instruction if it's not alread added
		if (stristr( m_pcb->piBuf,pzPI ) == 0)
			strcpy( m_pcb->piBuf,pzPI );
	}
	void PreAllocReturnReesults(int nSize) // estimate the size that you will be returning before calling AppendResults() or using the overloaded <<
	{
		m_pcb->arg[0].Int = nSize;
		m_Exec(m_pcb,"pre"); // concatinate into send queue
	}
	void AppendResults(const char *pzString) // appends from pzString up to the first null
	{
		m_pcb->arg[0].CChar = pzString;
		m_Exec(m_pcb,"cat"); // concatinate into send queue
	}
	void AppendResults(const char *pzString, int nLen) // appends from pzString (can be binary) nLen bytes
	{
		m_pcb->arg[0].Int = nLen;
		m_pcb->arg[1].CChar = pzString;
		m_Exec(m_pcb,"wrt"); // concatinate into send queue
	}

	int GetTotalBytesIn()
	{
		m_Exec(m_pcb,"tot");			
		return  m_pcb->arg[0].Int;
	}

	int GetExpectedDataSize()
	{
		m_Exec(m_pcb,"siz");			// get the size of the expected transfer
		return  m_pcb->arg[0].Int;
	}
	int SendDataChunk( char *pData, int nDataLen )
	{
		m_pcb->arg[0].Char = pData;
		m_pcb->arg[1].Int = nDataLen;
		m_Exec(m_pcb,"xtx"); //Transmit
		return m_pcb->arg[0].Int; // -1 is fail, 1 is success
	}
	int NextDataChunk(int *pnDataSize)
	{
		m_Exec(m_pcb,"xrx");// Receive
		*pnDataSize = m_pcb->arg[1].Int;
		return m_pcb->arg[0].Int; // -1 = error, 0 = no data here yet, 1 got it
	}
	int NextDataChunk(int *pnDataSize, char *pDest, int nMax, int nSecondsTimeout, int nMicrosecondsTimeout)
	{
		m_pcb->arg[0].Int = -1;
		m_pcb->arg[1].Char = pDest;
		m_pcb->arg[2].Int = nMax;
		m_pcb->arg[3].Int = nSecondsTimeout;
		m_pcb->arg[4].Int = nMicrosecondsTimeout;

		m_Exec(m_pcb,"xrx");// Receive
		*pnDataSize = m_pcb->arg[1].Int;
		return m_pcb->arg[0].Int; // -1 = error, 0 = no data here yet, 1 got it
	}
	int GetArgumentSize(int nIndex = 1) // 1 based index, 1 is the 1st arg, 2 is the 2nd
	{
		m_pcb->arg[0].Int = nIndex;
		m_Exec(m_pcb,"map");
		return m_pcb->arg[0].Int;
	}
	char *GetUserName() 
	{
		m_pcb->arg[0].Char = m_StringResultBuffer;
		m_pcb->arg[1].Int = sizeof(m_StringResultBuffer);
		m_Exec(m_pcb,"usr");
		return 	m_StringResultBuffer;
	}
	char *MessageArgs(int nIndex) // only available in Messaging protocol
	{
		m_pcb->arg[0].Int = nIndex;
		m_pcb->arg[1].Char = m_StringResultBuffer;
		m_pcb->arg[2].Int = sizeof(m_StringResultBuffer);
		m_Exec(m_pcb,"msg");
		return 	m_StringResultBuffer;
	}
	char *GetCallerIPAddress()
	{
		m_pcb->arg[0].Char = m_StringResultBuffer;
		m_Exec(m_pcb,"ip");
		return 	m_StringResultBuffer;
	}
	int GetServerThreadID()
	{
		m_Exec(m_pcb,"tid");
		return m_pcb->arg[0].Int;
	}
	void Disconnect()
	{
		m_Exec(m_pcb,"dis");
	}
	int GetServerSocketHandle()
	{
		m_Exec(m_pcb,"fd");
		return m_pcb->arg[0].Int;
	}
	int GetKeepAliveCount()	// this only applies to plugins called by the HTTP Server
	{
		m_Exec(m_pcb,"keepalive");
		if (m_pcb->arg[0].PInt)
			return *m_pcb->arg[0].PInt;
		return 0; // this must be protocol Xfer if the Keep Alive Count is not set
	}
	int SetKeepAliveCount(int nNewCount) // this only applies to plugins called by the HTTP Server
	{
		m_Exec(m_pcb,"keepalive");
		*m_pcb->arg[0].PInt = nNewCount;
	}
	char *GetBuffer1()
	{
		return (char *)m_pcb->wkBuf;
	}
	char *GetBuffer2()
	{
		return (char *)m_pcb->wkBuf2;
	}
	int GetBufferSize()
	{
		return m_pcb->nWKBufferSize;
	}

	PlugInController & operator<<(const char *p)
	{
		AppendResults(p);
		return *this;
	}
	PlugInController & operator<<(int n)
	{
		char  szBuffer[25];
		sprintf(szBuffer, "%li", n);
		AppendResults(szBuffer);
		return *this;
	}
	PlugInController & operator<<(const GString &g)
	{
		AppendResults(g, g.Length());
		return *this;
	}

/*
	PlugInController & operator<<(__int64);
	PlugInController & operator<<(const signed char *);
	PlugInController & operator<<(char);
	PlugInController & operator<<(unsigned char);
	PlugInController & operator<<(signed char);
	PlugInController & operator<<(short);
	PlugInController & operator<<(unsigned short);
	PlugInController & operator<<(unsigned int);
	PlugInController & operator<<(long);
	PlugInController & operator<<(unsigned long);
	PlugInController & operator<<(float);
	PlugInController & operator<<(double);
	PlugInController & operator<<(long double);
	PlugInController & operator<<(const GString &);
*/

};





class FormPart
{
public:
	GString strDestFileName;
	GString strSourceFileName;
	GString strPartName;
	GString strHeader;
	GString *strData;
	int m_bOwns;
	FormPart(int nEstimatedSize) {strData = new GString(nEstimatedSize); m_bOwns = 1; };
	FormPart(GString *pDest) {strData = pDest; m_bOwns = 0; };
	~FormPart() {if (m_bOwns == 1) delete strData; };
};

class CMultiPartForm
{
	PlugInController *m_pPIC;
	char *m_pzContent;
	int m_pzContentIndex;
	int m_nBytesInBuffer;
	GString m_strBoundary;
	GString m_strEndBoundary;
	unsigned int m_nRemainingData;
	int m_bFinished;
	GBTree m_Parts;
	int m_bOwnMemory;
	int m_nLastError;
	GString m_strLastError;
public:
	~CMultiPartForm()
	{
		if (m_nLastError != 0)
		{
			// if m_nLastError != 0, then we didn't finish reading the POST data so the browser is all queued up 
			// and waiting to send more, and won't even consider looking for a response to the POST until it's
			// finished sending it.  This can only happen 2 ways.  1) either we failed to write to our local disk
			// and stopped reading the POST data because there was nowhere to put it, or 2) someone POSTed us garbage.
			// Either way, disconnect is appropriate.
			m_pPIC->Disconnect();
		}
		if (m_bOwnMemory)
		{
			delete m_pzContent;
			m_bOwnMemory = 0;
		}
		GBTreeIterator it(&m_Parts);
		while(it())
		{
			delete (FormPart *)it++;
		}
	}
	
	CMultiPartForm(PlugInController *pPIC, const char *pzHeader, const char *pzContent)
	{
		m_pzContent = (char *)pzContent;
		m_pPIC = pPIC;
		char *p = stristr(pzHeader,"boundary=");
		if (p)
		{
			m_strBoundary.SetFromUpTo(p+9,"\r\n");
			m_strEndBoundary << "\r\n--" << m_strBoundary;
		}
		m_nBytesInBuffer = m_pPIC->GetTotalBytesIn();
		m_nRemainingData = m_pPIC->GetExpectedDataSize() - m_nBytesInBuffer;
		m_pzContentIndex = 0;
		m_bFinished = 0;
		m_bOwnMemory = 0;
		m_nLastError = 0;
	}
	void MapArgument(const char *pzArgName, GString *pstrDest)
	{
		FormPart *p = new FormPart(pstrDest);
		p->strPartName = pzArgName;
		m_Parts.insert(pzArgName,p);
	}
	void MapArgumentToFile(const char *pzArgName, const char *pzPathAndFileName)
	{
		FormPart *p = new FormPart(32767);
		p->strDestFileName = pzPathAndFileName;
		p->strPartName = pzArgName;
		m_Parts.insert(pzArgName,p);
	}
	FormPart *AddArgument(const char *pzArgName, int nEstimatedSize = 256)
	{
		FormPart *p = new FormPart(nEstimatedSize);
		p->strPartName = pzArgName;
		m_Parts.insert(pzArgName,p);
		return p;
	}
	const char *GetLastError(int *pnErrorCode = 0)
	{
		if (pnErrorCode)
			*pnErrorCode = m_nLastError;
		return m_strLastError;
	}
	int GetAll()
	{
		while(ReadNext(0));
		return (m_bFinished) ? 1 : 0; // 1 = success, 0 = error
	}

	int ReadNext(FormPart **ppPart)
	{
		int nReadMore = 0;
RETRY:
		int nSuccess = 0;
		FormPart *pFormPart = 0;
		int bIsDiskFile = 0;
		char *pStart = &m_pzContent[m_pzContentIndex];

		if (memcmp(&pStart[2], m_strBoundary.Buf(), m_strBoundary.Length()) == 0)
		{
			GString strName;
			GString *pHeader = new GString;
			int nBoundryLen = m_strBoundary.Length();
			if ( pHeader->SetFromUpTo(pStart + nBoundryLen, "\r\n\r\n") == -1)
			{
				if (pStart[nBoundryLen+2] == '-' && pStart[nBoundryLen+3] == '-')
				{
					// we successfully read all the way to the termination boundry marker
					m_bFinished = 1;
					return 0;
				}
				else
				{
					// it is possible that the last network read split the multipart subheader
					// otherwise this is not valid HTTP
					nReadMore++;
				}
			}
			else
			{
				char *p = stristr(pHeader->Buf(),"name=");
				if (p)
				{
					char *pQuote = strpbrk(p,"\"");
					if (pQuote)
					{
						strName.SetFromUpTo(pQuote+1,"\"");
					}
				}
			}

			if (strName.Length())
			{
				pFormPart = (FormPart *)m_Parts.search(strName);
				if (!pFormPart)
				{
					pFormPart = AddArgument(strName);
				}
				char *p = stristr(pHeader->Buf(),"filename=");
				if (p)
				{
					char *pQuote = strpbrk(p,"\"");
					if (pQuote)
					{
						pFormPart->strSourceFileName.SetFromUpTo(pQuote+1,"\"");
					}
				}


				int nOffset = m_strBoundary.Length() + 2 + pHeader->Length() + 2;
				bIsDiskFile = pFormPart->strDestFileName.Length();
				if (bIsDiskFile)
				{
					unlink(pFormPart->strDestFileName);  // if the destination file existed we would otherwise append to it without this line of code - that could be desired behavior in rare cases
				}

				int nLen = m_strEndBoundary.Length();
				while(1)
				{
					if ( memcmp(&pStart[nOffset],m_strEndBoundary, nLen) == 0)
					{
						// finished.
						m_pzContentIndex += ( nOffset );
						m_pzContentIndex += 2;
						nSuccess = 1;
						break;
					}
					else
					{
						pFormPart->strData->write( &pStart[nOffset++], 1 );
					}


					if (nOffset + m_pzContentIndex  == m_nBytesInBuffer)
					{
						// we could not set it, get more data if there is more
						if ( m_nRemainingData > 0 )
						{
							if (bIsDiskFile)
							{
								if ( !pFormPart->strData->ToFileAppend( pFormPart->strDestFileName, 0) )
								{
									m_strLastError = "Failed to write to file: ";
									m_strLastError += pFormPart->strDestFileName;
									m_nLastError = 1000;
									break;
								}
								else
								{
									*pFormPart->strData = ""; // reset the GString to 0 size, retain the memory allocation
								}
							}

							int nRslt = m_pPIC->NextDataChunk(&m_nBytesInBuffer);
							if (nRslt == -1)
							{
								// the connection has been broken
								m_strLastError = "Connection to client has been broken or it timed out";
								m_nLastError = 1001;
								break;
							}
							else if (nRslt == 1)
							{
								m_nRemainingData -= m_nBytesInBuffer;
								pStart = m_pPIC->GetBuffer1();
								pStart[m_nBytesInBuffer] = 0;
								if (m_bOwnMemory)
								{
									delete m_pzContent;
									m_bOwnMemory = 0;
								}
								m_pzContent = pStart;
								nOffset = 0;
								m_pzContentIndex = 0;
								continue;
							}
							else if (nRslt == 0)
							{
								// there is no more data here yet
							}
						}
						break;
					}
				}
			}
		}
		else
		{
			nReadMore++;
		}

		// if m_nBytesInBuffer - m_pzContentIndex is > 512, there is no chance that we split the subheader
		// between network reads, rather this means that the HTTP is malformed
		if (nReadMore == 1 && ((m_nBytesInBuffer - m_pzContentIndex) < 512)  )
		{
			// The following comment is risky code, because it intentionally writes past the end of the system buffer.
			// It's safe as long as MAX_RAW_CHUNK is 16384 or bigger because it uses the grow space
			// defined as (MAX_RAW_CHUNK / 32), but if a server is built with small buffers, this would be bad
			//
			// int nDataSize = 0;
			// NextDataChunk(&nDataSize, &m_pzContent[m_nBytesInBuffer], 512, 60, 0);
			// m_nBytesInBuffer += nDataSize;
			//
			// so we'll do it the way it does not create a kernel build dependancy
			// this copies the tail of the last network read into our memory and sets all indexes relative to the 
			// new memory and now we also have to be sure to delete it, but that's not so simple because it may need to
			// exist until the next multi-part section gets parsed, so the delete has to exist in 3 places, 
			// search this file for "m_bOwnMemory" to see all three places, that could be removed if we depended on the kernel build settings.
			int nBufferTailSize = m_nBytesInBuffer - m_pzContentIndex;
			char *pBuf = new char[ nBufferTailSize + 512 ];
			memcpy(pBuf,&m_pzContent[m_pzContentIndex], nBufferTailSize);
			int nDataSize = 0;
			m_pPIC->NextDataChunk(&nDataSize, &pBuf[nBufferTailSize], 511, 60, 0); // 511 save room for the null
			m_nBytesInBuffer = nDataSize + nBufferTailSize;
			pBuf[m_nBytesInBuffer] = 0;
			if (m_bOwnMemory)
			{
				delete m_pzContent;
			}
			m_bOwnMemory = 1;
			m_pzContent = pBuf;
			m_pzContentIndex = 0;


			// now, we need to try it again
			goto RETRY;
		}
		if (nReadMore > 1)
		{
			m_strLastError = "The format of the HTTP multipart is invalid";
			m_nLastError = 1002;
		}
		
		if (nSuccess)
		{
			if (ppPart)
				*ppPart = pFormPart;

			if (bIsDiskFile)
			{
				if ( !pFormPart->strData->ToFileAppend( pFormPart->strDestFileName, 0) )
				{
					// error writing to disk
				}
				else
				{
					pFormPart->strData->Reset();
				}
			}

			m_strLastError = "The command was successful";
			m_nLastError = 0;
			return 1; // success
		}
		return 0; 
	}
};

