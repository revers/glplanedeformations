// --------------------------------------------------------------------------
//					www.UnitedBusinessTechnologies.com
//			  Copyright (c) 1998 - 2001  All Rights Reserved.
//
// Source in this file is released to the public under the following license:
// --------------------------------------------------------------------------
// This toolkit may be used free of charge for any purpose including corporate
// and academic use.  For profit, and Non-Profit uses are permitted.
//
// This source code and any work derived from this source code must retain 
// this copyright at the top of each source file.
// 
// UBT welcomes any suggestions, improvements or new platform ports.
// email to: XMLFoundation@UnitedBusinessTechnologies.com
// --------------------------------------------------------------------------
#ifdef _WIN32

	// Win32 threading
	#include <wtypes.h>		
	#include <winbase.h>
	
	// C runtime includes
	#include <wtypes.h>		
	#include <winbase.h>
	#include <assert.h>
	#include <sys/timeb.h>
	#include <time.h>
	#include <fstream.h>
	#include <stdio.h>
	#include <fstream.h>
	#include <stdarg.h>
	#include <string.h>
	#include <ctype.h>
	#include <math.h>		
	#include <stdlib.h>		

	// Generic utility classes
	#include "GString.h"
	#include "GList.h"
	#include "GHash.h"

#endif
