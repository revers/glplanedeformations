// --------------------------------------------------------------------------
//						United Business Technologies
//			  Copyright (c) 2000 - 2009  All Rights Reserved.
//
// Source in this file is released to the public under the following license:
// --------------------------------------------------------------------------
// This toolkit may be used free of charge for any purpose including corporate
// and academic use.  For profit, and Non-Profit uses are permitted.
//
// This source code and any work derived from this source code must retain 
// this copyright at the top of each source file.
// --------------------------------------------------------------------------

// used by XMLObject::MapMember() when a wrapper tag is specified.
#ifndef _RELATIONSHIP_WRAP__
#define _RELATIONSHIP_WRAP__

#include "MemberDescriptor.h"
class XMLRelationshipWrapper : public XMLObject
{
	XMLRelationshipWrapper(){/*never used*/}
	
	// 1=list, 2=string list, 3=int array, 4=object, 5=objPointer, 6=KeyedDataStructure
	int m_nContainedType; 

	// abstract the abstraction specified by m_nContainedType
	void *m_pData;
	void *m_pAccessor;
public:
	XMLRelationshipWrapper(const char *pzTag)
	{	
		SetDefaultXMLTag( pzTag );
		SetObjectTypeName( pzTag );
		m_pDerivedAddress = this;
		setMemberStateSummary(DATA_DIRTY);
	}

	~XMLRelationshipWrapper()
	{
		// added 12/28/2009 because a list wrapped in a RelationShipWrapper was decrementing twice in ~XMLObject
		// once in ReferenceChangeChildren(), then a second time when this owning object was destroyed
		ModifyObjectBehavior(PREVENT_AUTO_DESTRUCTION,0);
	};
	
	virtual bool SerializeObject()
	{
		bool bRet = true;
		switch (m_nContainedType)
		{
		case 1: // don't write the wrapper if the wrapped list is empty
		bRet = (((ListAbstraction *)m_pAccessor)->itemCount(m_pData) == 0) ? false : true;
		break;
		case 2: // likewise if there are no strings in a string list
		bRet = (((StringCollectionAbstraction *)m_pAccessor)->itemCount(m_pData) == 0) ? false : true;
		break;
		case 3: 
		bRet = (((IntegerArrayAbstraction *)m_pAccessor)->itemCount(m_pData) == 0) ? false : true;
		break;
		case 4: // always write a sub object unless it specifically says not to
		bRet = ((XMLObject *)m_pData)->SerializeObject();
		break;
		case 5: // the same for object pointers if they've been allocated
		if (m_pData && *((XMLObject **)m_pData) && (*((XMLObject **)m_pData))->SerializeObject()  )
			bRet = true;
		else
			bRet = false;
		break;
		case 6:
			bRet = (((KeyedDataStructureAbstraction *)m_pAccessor)->itemCount(m_pData) == 0) ? false : true;
		break;
		}
		
		return bRet;
	};
	
	// XMLRelationshipWrapper is a pseudo-Object used for grouping like members.
	// this is used only by XMLObject to build "wrapper classes"
	// AddReference is overloaded for each "mappable-wrappable" type.
	void AddReference(const char *pzTag, void *pList,ListAbstraction *pHandler,ObjectFactory pFactory, int bFirstMap)
	{
		m_nContainedType = 1;
		m_pData = pList;
		m_pAccessor = pHandler;

		MapMember(pList,pzTag,pHandler,0,pFactory);
		MemberDescriptor *pMD = (MemberDescriptor *)m_lstMembers.Last(); 
		pMD->m_bFirstMap = bFirstMap;
	}
	void AddReference(const char *pzTag, XMLObject **ppObj, ObjectFactory pFactory )
	{
		m_nContainedType = 5;
		m_pData = ppObj;

		MapMember(ppObj,pzTag,0,pFactory);
	}
	void AddReference(const char *pzTag, XMLObject *pObj )
	{
		m_pData = pObj;
		m_nContainedType = 4;

		MapMember(pObj,pzTag);
	}
	void AddReference(void *pStringCollection, const char *pzElementName, StringCollectionAbstraction *pHandler)
	{
		m_nContainedType = 2;
		m_pData = pStringCollection;
		m_pAccessor = pHandler;

		MapMember( pStringCollection,pzElementName, pHandler, 0);
	}
	void AddReference(void *pIntegerArray, const char *pzElementName, IntegerArrayAbstraction *pHandler)
	{
		m_nContainedType = 3;
		m_pData = pIntegerArray;
		m_pAccessor = pHandler;

		MapMember( pIntegerArray,pzElementName, pHandler, 0);
	}
	
	void AddReference( void *pDataStructure, KeyedDataStructureAbstraction *pHandler, 
						const char *pObjectName)
	{
		m_nContainedType = 6;
		m_pData = pDataStructure;
		m_pAccessor = pHandler;

		MapMember( pDataStructure, pHandler, pObjectName, 0);
	}
	
	virtual void MapXMLTagsToMembers(){}
};

#endif //_RELATIONSHIP_WRAP__