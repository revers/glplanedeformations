// --------------------------------------------------------------------------
//						United Business Technologies
//			  Copyright (c) 2000 - 2009  All Rights Reserved.
//
// Source in this file is released to the public under the following license:
// --------------------------------------------------------------------------
// This toolkit may be used free of charge for any purpose including corporate
// and academic use.  For profit, and Non-Profit uses are permitted.
//
// This source code and any work derived from this source code must retain 
// this copyright at the top of each source file.
// --------------------------------------------------------------------------

#ifndef _XML_BASE_OBJECT_SUPPORT__
#define _XML_BASE_OBJECT_SUPPORT__

#include "xmlDefines.h"
#include "CacheManager.h"
#include "GException.h" // because XMLObject throws these in FromXML
#include "MemberDescriptor.h" // because most compilers allow the declaration for [m_EntryRoot] but gcc does not unless it has the entire definition rather than a just a forward declaration which is all that ought to be necessary.
#include "GHash.h" // for m_hshActiveMaps

#include <stdio.h> // for IMPLEMENT_FACTORY macro: sprintf()

class StackFrameCheck;
class XMLRelationshipWrapper;
class XMLObject;
class StringAbstraction;
class MemberHandler;
class XMLObjectDataHandler;
class XMLAttributeList;
class GString;
class hashTbl;

class GQSortArray;
class GArray;

// global reference to the abstraction instance
class XMLObject
{
	int m_ValidObjectBeg;

	// Runtime type information
	GString *m_strObjectType;		// derived class name.
	GString *m_strXMLTag;			// default tag for this instance

	// MemberDescriptor is a structure that describes each member
	friend class MemberDescriptor;

	// Storage is allocated only when necessary to keep the XMLObject lightweight.
	GString *m_oid;
	GList   *m_lstOIDKeyParts;
	GString *m_TimeStamp;
	GString *m_pToXMLStorage;

	// Custom object data handler - see <ObjectDataHandler.h> for full
	// description and example.
	XMLObjectDataHandler *m_pDataHandler;
	
	// storage of Attribute mapping rules 
	GHash				*m_pMappedAttributeHash;

	// see GetObjectBehaviorFlags() in this header file
	unsigned int m_nBehaviorFlags;

	// see comment for setMemberStateSummary() in this header file.
	// object/member states are defined in xmlDefines.h
	unsigned int m_nMemberStateSummary;

protected:
	// storage for unmapped attributes is allocated only when necessary
	XMLAttributeList	*m_pAttributes;

	// The following members are set by the derived class during construction
	void *m_pDerivedAddress;	    // == the 'this' of derived class
	ObjectFactory m_classFactory;   // Pointer to factory for this object type
	void SetObjectTypeName(const char *);// sets m_strObjectType ex. 'CCustomer'
	void SetDefaultXMLTag(const char *);	// sets m_strXMLTag ex. 'Customer'

	// if this object was mapped by another object or
	// if this object is mapped into a list of another object
	// this list will contain the MemberDescriptor that
	// was used to create 'this' object, or the MemberDescriptor
	// that describes the containment relationship if 'this' was
	// not factory created.
	//
	// If multiple instances are active, this list holds all the maps
	// for each instance type, not of each instance.
	GList m_lstCreationDescriptors;

	// Member-to-Tag mapping tree root for alphabetical indexing by xml tag
	MemberDescriptor *m_EntryRoot;
	// Create member description structure and add to indexed structures
	void RegisterMember(MemberDescriptor *);
	// search based on xml tag for a member - used by the ObjectFactory
	MemberDescriptor *GetEntry( MemberDescriptor* btRoot, 
													const char * szToFind );
	MemberDescriptor *GetEntry( MemberDescriptor* btRoot, 
													void * pToFind );
	long m_nRefCount;

private:
	
	// If MapMember was called to store objects in GList, GHash or types supporting 
	// deferred destruction.  XMLFoundation can auto-cleanup all the objects it 
	// created - now it is done and it's time to cleanup the data structure
	void ExecuteDeferredDestructions();

	// see comment above XMLObject::IsFirstMap() in XMLObject.cpp
	GList m_lstDataStructures;
	int IsFirstMap( void *pList );

	MemberDescriptor *m_MRUObjectDescriptor;

	bool AddEntry( MemberDescriptor **Root,MemberDescriptor *ToAdd );

	// Storage for objects used to describe object relationships in XML
	GList *m_pRelationShipWrappers;// contains XMLRelationshipWrapper's

	void WriteObjectEndTag( GString& xml, int nTabs, const char *TagOverride = 0, 
								unsigned int nSerializeFlags = 0, int nXMLSize = 0 );
	int WriteObjectBeginTag( GString& xml, int nTabs, const char *TagOverride = 0, 
								unsigned int nSerializeFlags = 0 );

	// debugging support private methods
	void FormatMemberToDump(MemberDescriptor *pMD, GString &strDest, 
										int nTabs, StackFrameCheck *pStack, const char *pzPlace);
	void DumpMember( MemberDescriptor* btRoot, GString &strDest, 
										int nTabs, StackFrameCheck *pStack);

protected:
	// List of nodes in a tree (alternate index to the same data) for 
	// sequential access and preservation of order in which MapMember() 
	// was called for full control of member location in the serialized XML.
	GList m_lstMembers; // contains MemberDescriptor;s

	// Have object "subscribe to data" not contained in member variables
	// see comment in ObjectDataHandler.h for full example.
	void setObjectDataHandler( XMLObjectDataHandler * p );

	// Forces a call to MapXMLTagsToMembers() if it has not already been done.
	void LoadMemberMappings();

	virtual void MapXMLTagsToMembers(){};
	virtual const char *GetVirtualTag(){return 0;};
	

	// internal helper functions to the ToXML* methods
	void GenerateSubsetXML(MemberDescriptor* btRoot,	
							GString& xml, int nTabs, StackFrameCheck *pStack,
							int nSerializeFlags, const char *pzSubsetOfObjectByTagName);

	void GenerateOrderedXML(MemberDescriptor* btRoot,	
							GString& xml, int nTabs, StackFrameCheck *pStack,
							int nSerializeFlags);
	void GenerateMappedXML(MemberDescriptor* btRoot,	
							GString& xml, int nTabs, StackFrameCheck *pStack,
							int nSerializeFlags);

	// Provides advanced mapping control to the object derived from XMLObject.  
	// Beware not to over-use a feature like this to solve situations that are 
	// more appropriately solved with calls to SetMemberSerialize() or by 
	// adjusting flags in the serialization. 
	// Think of it as a 'goto' in 'C++', it can be overused.
	// After a call to UnMapMembers(), the next operation on this object that
	// uses the object data description will re-execute MapXMLTagsToMembers().
	void UnMapMembers( MemberDescriptor* btRoot = 0);

	// Called from Release() to recursively release children
	void ReferenceChangeChildren(StackFrameCheck* pStack, int nChange);
	void ApplyToChildren(MemberDescriptor *pMD, StackFrameCheck* pStack, int nChange);

private:
	int m_ValidObjectEnd;
public:

	XMLObject();

	// assign all the members possible in this object from the source object
	int CopyState(XMLObject *pCopyFrom);
	
	// special behavior - uses "object behavior flags" defined in xmlDefines.h
	inline unsigned int GetObjectBehaviorFlags(){return m_nBehaviorFlags;}
	void ModifyObjectBehavior(unsigned int nAdd, unsigned int nRemove = 0)
	{
		m_nBehaviorFlags = (m_nBehaviorFlags & ~nRemove) | nAdd;
	}

	// state attachment/detatchment from StateCache
	void ReStoreState(long oid);
	void ReStoreState(int oid);
	void ReStoreState(const char * oid);
	void StoreState();

	// called after the object members have been initialized from the XML during 
	// a call to FromXML(), it is called EACH time the object is updated.
	virtual void Construct() {};
	// called by the XMLObjectFactory when OID's are pre-specified in the attribute
	// list of the source XML data stream. Faster -  no temp objects are used.
	void Init(const char* m_oid, const char* pzTimeStamp );
	// called by the XMLObjectFactory on the behavior flag EARLY_CONTAINER_NOTIFY
	void EarlyNotify();


	// Access to attributes of the object, if nUpdate=1 the attribute list will
	// be searched and update if there is an existing attribute called [pzName]
	// if not found or if nUpdate=0 the new attribute will be added.
	void AddAttribute( const char * pzName, int nValue, int nUpdate=0 );
	void AddAttribute( const char * pzName, const char * pzValue, int nUpdate=0 );
	const char *FindAttribute( const char *pzAttName );
	void RemoveAllAttributes();

	// set attributes of members, rarely used - should your member be an object?
	void AddMemberAttribute( const char *pzMemberTag, const char * pzName, int nValue );
	void AddMemberAttribute( void * pMemberAddress,   const char * pzName, int nValue );
	void AddMemberAttribute( const char *pzMemberTag, const char * pzName, const char * pzValue );
	void AddMemberAttribute( void * pMemberAddress,   const char * pzName, const char * pzValue );

	// Reach out to the farthest derivation and put that 'this' pointer
	// into m_pDerivedAddress, so that through a template accessor or
	// static cast, specifying the derived TYPE we may downcast 'that',
	// (that is cast from derived toward the base), either base in the 
	// case of multiple inheritance.  COM/CORBA use MI.
	virtual void RegisterObject(){}
	virtual const char *GetVirtualType(){return 0;};

	// This resolves implicit upcasting to the object that is multiply
	// derived from CORBA::Object and XMLObject. When called from the
	// derived class the value returned will equal the 'this' pointer
	// of the outermost derived class, that can be then safely downcast
	// to any one of it's multiply inherited base classes.
	void *GetUntypedThisPtr() {return m_pDerivedAddress;}
	void *GetInterfaceObject() {if (!m_pDerivedAddress) RegisterObject(); return m_pDerivedAddress;}
	virtual ~XMLObject();

	// Returns true if any child member in this object or any sub object
	// has a state of dirty.
	int CalculateObjectState( MemberDescriptor* btRoot = 0 );
	
	// access to runtime type and bound XML tag.
	const char *GetObjectType();// type as 1st param to IMPLEMENT_FACTORY					
	const char *GetObjectTag();	// xml tag as 2nd param to IMPLEMENT_FACTORY
	const char *GetXMLTag(){return GetObjectTag();}// less direct, always equal

	// If the runtime instance of 'this' is contained in an object that
	// used MapMember() to define the type relationship, then calling
	// GetParentObject() will return a pointer to the instance of the owner.
	XMLObject *GetParentObject();
	// if the refcount on 'this' is > 1 and the 'owning' objects are not the same
	// for example this 'Item' is on several 'Invoice' objects all referring to 
	// the same 'this'(Item), this method will allow you to call a 'Changed'
	// method on all active parent objects that may need to make screen updates.
	GList *GetParentObjects(GList *lstResults); // returns lstResults

	// Loads m_lstOIDKeyParts:
	// Map a set of unchanging data on this object that uniquely identifies it
	// between all objects of this type.  This will be called from your 
	// MapXMLTagsToMembers() only once.  It is optional if you want automatic
	// cache management. ObjectID's (oid's) may consist of 1 to 5 key parts 
	// that can be elements, attributes, or a combination of both.  
	// nSource=1 for Elements, or 0 is the preceeding tag is an Attribute
	void MapObjectID(const char *pTag1,   int nSource1, 
					 const char *pTag2=0, int nSource2=0,
					 const char *pTag3=0, int nSource3=0,
					 const char *pTag4=0, int nSource4=0,
					 const char *pTag5=0, int nSource5=0);
	
	// map an unknown, let the derived class handle this member variable
	// see: MemberHandler.h for a complete description of this functionality
	void MapEntity(const char *pzEntityName);
	void MapMember(const char *pTag, MemberHandler *pHandler);

	// Called by the Object Factory when assigning Attributes values to 'this'
	int SetMappedAttribute(const char *pTag,const char *pzValue, int nValueLen);
        
        // Revers:
	void MapAttribute(float *pValue,const char *pTag);
	void MapAttribute(double *pValue,const char *pTag);
        
	// Map an integer
	void MapAttribute(int *pValue,const char *pTag);
	// Map a string, see StringAbstraction.h for interface and samples
	void MapAttribute(GString *pstrValue,const char *pTag);
	void MapAttribute(void *pValue,const char *pTag,StringAbstraction *pHandler);

        //Revers:
        void MapMember(float *pValue,const char *pTag);
        void MapMember(double *pValue,const char *pTag);
        
	// Map an int / long int / or very long int
	void MapMember(int *pValue,const char *pTag);
	void MapMember(long *pValue,const char *pTag);
	void MapMember(__int64 *pValue, const char *pTag);

	// Map a string, see StringAbstraction.h for interface and samples
	void MapMember(GString *pValue,const char *pTag);
	void MapMember(void *pValue,const char *pTag,StringAbstraction *pHandler);

	// Map an object into a hash table, binary tree, or QSort array
	// Note: You must Map an OID to use these.
	void MapMember(GHash *pDataStructure,const char *pzObjectName,	const char *pNestedInTag = 0);
	void MapMember(GBTree *pDataStructure,const char *pzObjectName,	const char *pNestedInTag = 0);
	void MapMember(GQSortArray *pDataStructure,const char *pzObjectName,const char *pNestedInTag = 0);
	void MapMember(void *pDataStructure,KeyedDataStructureAbstraction *pHandler,const char *pzObjectName,const char *pNestedInTag = 0);

	// Map a collection of Strings
	void MapMember(GStringList *pStringCollection,	const char *pzElementName,const char *pNestedInTag = 0);
	void MapMember(void *pStringCollection,	const char *pzElementName,StringCollectionAbstraction *pHandler,const char *pNestedInTag = 0);

	// Map a dynamically growing Integer array
	void MapMember(GArray *pIntegerArray,	const char *pzElementName,const char *pNestedInTag = 0);
	void MapMember(void *pIntegerArray,	const char *pzElementName,IntegerArrayAbstraction *pHandler,const char *pNestedInTag = 0);

	// Map a collection of Objects in a List
	void MapMember(void *pList,const char *pObjectTag,ListAbstraction *pHandler,const char *pNestedInTag = 0, ObjectFactory pFactory = 0 );
	void MapMember(GList *pList,const char *pObjectTag,const char *pNestedInTag = 0, ObjectFactory pFactory = 0 );

	// Map a sub-object using a tag other than defined in the DECLARE_Factory
	void MapMember(XMLObject *pObj,	const char *pDefaultTagOverride = 0, const char *pzWrapper = 0 );
	// Map a sub-object pointer to an object residing in the ObjectCache
	void MapMember(XMLObject **pObj,const char *pzTag, const char *pNestedInTag= 0, ObjectFactory pFactory=0);


	// Allows the default state of 'true' for member serialization to be changed.
	// This is how backward compatibility is achieved when Element tag
	// names change.  For example, if m_nVersion is mapped to an Element
	// named "VersionNumber" but you want all future protocols refer to this 
	// Element as to "ProtocolVersion".  This is achieved by the following code:
	// (...)
	//MapMember(&m_nVersion,"VersionNumber");
	//MapMember(&m_nVersion,"ProtocolVersion");
	//SetMemberSerialize("VersionNumber", false );
	// (...)
	// This allows either "VersionNumber" or "ProtocolVersion" to set the value of 
	// m_nVersion, but always refers to it as "ProtocolVersion" while serializing XML.
	void SetMemberSerialize(const char *pTag, bool bSerialize);
	
	// override this to conditionally include an object/subobject in ToXML() results.
	// see RelationshipWrapper.h for implementation example.
	virtual bool SerializeObject() {return true;};


	// given the address of a member, return it's XML Tag defined in MapXMLTagsToMembers()
	const char *GetMemberTag(void *pAddressOfMemberToSet);

	// call this to mark an object for serialization, for example if a 
	// list of inventory objects is loaded from XML, and several new inventory
	// objects are created by the user and added to the list, calling
	// setObjectDirty() on each new inventory object allows you to serialize
	// the list and write out only the 'new' objects possibly for a DBMS insert.
	// If the bAllMembers flag is '1', then every member in this object is also
	// set as dirty.
	void setObjectDirty(int bAllMembers = 0);

	// When objects are populated from the XML stream, they have a state of
	// Not dirty and Not null.  When the members are assigned by the derived 
	// class through SetMember() or SetMemberByTag() the state becomes dirty.
	// When using the SetMember() or SetMemberByTag() there is no need to 
	// setMemberDirty(), but if the derived class does direct assignments
	// to members that should be serialized, setMemberDirty() should be called.
	// if you ever want to serialize or track the state change only.
	// Setting bDirty to 0 will clear the dirty flag for a member variable.
	bool setMemberDirty(void *pAddressOfMemberToSet, int bDirty = 1);
	bool setMemberDirty(char *pzTagNameOfMemberToSet, int bDirty = 1);

	// true if the memory state does not sync with the 
	// original value set by the Object Factory
	bool isMemberDirty(void *pAddressOfMemberToCheck);
	bool isMemberDirty(char *pzTagNameOfMemberToCheck);

	// true if the member has never been assigned a value, it is uninitialized
	bool isMemberNull(void *pAddressOfMemberToCheck);
	bool isMemberNull(char *pzTagNameOfMemberToCheck);

	// true if the member has been assigned a value from the Object Factory
	bool isMemberCached(void *pAddressOfMemberToCheck);
	bool isMemberCached(char *pzTagNameOfMemberToCheck);

	// load this object and all it's child objects from an xml source
	// If a pSecondaryHandler is supplied it will be queried for storage
	// handlers for any items that are not mapped to this object or one of
	// it's child objects.  XMLProcedureCall is an example pSecondaryHandler.
	// Throws an XMLException if anything goes wrong.  The optional pzErrorSubSystem
	// will prefix the Exception text if an XMLException is serialized in.
	// NOTE: Throws GException
	// (if you get a MSFT compile error here(2059 and 2238) move the #include <XMLFoundation.h> to your stdafx.h)
	void FromXML(const char *pzXML, XMLObject *pSecondaryHandler=0, const char *pzErrorSubSystem=0);
	void FromXMLFile(const char *pzFileName, XMLObject *pSecondaryHandler = 0,const char *pzErrorSubSystem = 0);
	
	// Same as FromXML() and FromXMLFile(), but it does NOT throw an exception.
	// this is true of any XMLObject method name that is followed by an X
	// returns 1 on success, 0 for failure
	// if you don't want error details, leave [pErrorDescriptionDestination] NULL
	// if [pErrorDescriptionDestination] points to a GString, that string will contain the error string
	int FromXMLX(const char *pzXML, GString *pErrorDescriptionDestination = 0, XMLObject *pSecondaryHandler = 0);
	int FromXMLFileX(const char *pzFileName, GString *pErrorDescriptionDestination = 0, XMLObject *pSecondaryHandler = 0);



	// ToXML*
	// Recurse this object structure to a linear buffer represented in XML
	// into a GString.  Any existing value in the GString will be replaced.
	// xml = Destination
	// nTabs = normally 0-the level of tab indentation of the lowest branch
	// TagOverride = null or the tag name of the root branch,
	// nSerializeFlags = any of the XML serialization flags
	// pStack = always null for public access
	// pzSubsetOfObjectByTagName = pipe seperated list of members to serialize
	//							   for example,"name|phone|id" otherwise null for all members
	//							   normally use SetMemberSerialize() rather than pzSubsetOfObjectByTagName
	// nPreAllocSize = estimated result size for better performance.
	const char * ToXML(int nPreAllocSize = 4096, unsigned int nSerializeFlags = FULL_SERIALIZE);
	virtual bool ToXML(GString* xml, int nTabs = 0, const char *TagOverride = 0,
							 unsigned int nSerializeFlags = FULL_SERIALIZE, 
							StackFrameCheck *pStack = 0, const char *pzSubsetOfObjectByTagName = 0);

	// Recurse this object structure to a linear buffer represented in XML into a disk file.
	// File is created or overwritten unless nAppend = 1
	// returns 1 for success, 0 for fail.  If bThrowOnFail is 1, an exception will be thrown on failure
	bool ToXMLFile(int nPreAllocateBytes, const char *pzPathAndFileName, bool bThrowOnFail = 1, int nAppend = 0);
	bool ToXMLFile(const char *pzPathAndFileName, int nTabs = 0, const char *TagOverride = 0,
					int nSerializeFlags = FULL_SERIALIZE, int nEstimatedBytes = 8192);

	// Called by the XML Parser to obtain storage for a tag value  Search by tag name
	virtual MemberDescriptor *GetEntry( const char * szTagToFind );
	// or by memory address.  For example:   GetEntry( &m_strFirstName );
	// to keep xml tag names central to one location-->MapXMLTagsToMembers()
	MemberDescriptor *GetEntry( void * pMemberToFind );


	// Contains summary state info, for example if ANY singe member is dirty
	// then the DATA_DIRTY bit will be on.  If ANY singe member is cached
	// then the DATA_CACHED bit will be on.  If you are interested in the 
	// state of a specific member, use the isMemberDirty() method.
	void setMemberStateSummary(int nBitFlag);
	bool getMemberStateSummary(int nBitFlag);

	// Set object member variables and maintain dirty state flag values
	void SetMember( void *pMemberToSet, const char *pzNewValue, 
										   MemberDescriptor* btRoot = 0 );
	void SetMember( void *pMemberToSet, int pzNewValue, 
										   MemberDescriptor* btRoot = 0 );

	// Generic Event Handler for custom object behavior. Abstract to reduce virtual method table
	// -----------------------------------------------------------------------------------------
	// nCase = "XMLAssign", (member tag, XML value, value len, null)
	//			when the FromXML() contains a value for a member set dirty by setMemberDirty()
	//			see MemberDescriptor::Set() in MemberDescriptor.cpp for details.
	// nCase = "NonNumeric" (member tag, XML value, value len, null)
	//			 when non-numeric XML data is mapped to an numeric only type.
	// nCase = "EmptyString"(member tag, XML value, value len, null)
	//			 when an empty("") value is assigned to a string, empty often differs from 'unknown' or 'unassigned'
	// nCase = "ObjectUpdate" when OBJECT_UPDATE_NOTIFY is a set behavior flag. (oid, null, flags, pObjSrc)
	// nCase = "XMLUpdate"    when OBJECT_UPDATE_NOTIFY is a set behavior flag. (oid, null, flags, pObjSrc)
	// nCase = "SubObjectUpdate" when SUBOBJECT_UPDATE_NOTIFY is a set behavior flag. (xml tag, null, flags, pObjSrc)
	// nCase = "MemberUpdate" when MEMBER_UPDATE_NOTIFY is a set behavior flag. (tag, value, valuelen, null)
	//
	// To turn on "ObjectUpdate" messages use this code in your constructor: ModifyObjectBehavior(OBJECT_UPDATE_NOTIFY);
	// You should return 0(NULL) for all message definitions as of 2009 - in the future new messages may require special return values
	virtual void *ObjectMessage( int nCase, const char *pzArg1, const char *pzArg2, unsigned int nArg3 = 0, void *pArg4 = 0 ){return 0;};


	// When a tag is encounterted that does not have a MapMember() entry
	// associated with it, this handler is called for the developer
	// to supply an "on-the-fly" MemberDescriptor during the Factory process.
	// This is usefull for dynamic objects. 
	virtual MemberDescriptor *HandleUnmappedMember( const char *pzTag );
	//	For example,
	// MemberDescriptor *YourObject::HandleUnmappedMember( const char *pzTag )
	// {

	// 	// This is the equivalent of a dynamic MapMember().  During the Factory
	// 	// creation process, we found a tag with no static map, and here we 
	// 	// will map it to a GString.
	// 	GString *pValue = new GString();
	// 	static GenericStringAbstract GSA;
    //
	//	MemberDescriptor *pMD = new MemberDescriptor(this, pzTag, pValue, &GSA);
	//	
	//	// place pMD in the base class (where the MemberDescriptor is normally created)
	//	// so it gets cleaned up when the object goes out of scope.
	//	RegisterMember(pMD);
	//
	//	// hang onto pValue since it was dynamically created by this class, it 
	//	// will get cleaned up by this class also.
	//	m_listDynamicStringMembers.AddLast(pValue);
	//	return pMD;
	// }



	//	Set the value of a member variable using it's XML Tag name as a unique identifier
	void SetMemberByTag( const char *pzTagName, const char *pzNewValue, bool bSetDirty = true);
	const char *GetMemberByTag( const char *pzTagName, GString &strDest );
	int GetMemberByTagInt( const char *pzTagName );

	// returns NULL or a string to uniquely identify this object among all instances of this type.
	const char *getOID();
	// returns NULL or a string that contains an object time stamp.
	const char *getTimeStamp();

	
	// the object descriptor that created, or references 'this'	
	int IsObjectDescriptor(MemberDescriptor *p);
	int GetObjectDescriptorCount();
	int SetObjectDescriptor(MemberDescriptor *p);
	MemberDescriptor *GetObjectDescriptor(); // returns Most Recently Used Descriptor

	XMLObjectDataHandler *GetObjectDataHandler() {return m_pDataHandler;}


	// assign a pre-built attribute list to an object that has no preexisting attributes
	void SetAttributeList(XMLAttributeList *p);

	// Dumps Entire Object state with all object associations to standard out
	const char *Dump();
	// Dumps Entire Object state with all object associations to a file
	// tip: use a good editor to view that allows you to press Ctrl-} 
	//      when your cursor is positioned on a brace'{' to find pairs of {}
	void Dump( const char *pzFileName, int bAppendToFile = 0 );
	// Append an Object state dump onto the supplied GString;
	void Dump(GString &strDest, int nTabs = 0, StackFrameCheck *pStack = 0);

	// Reference counting.
	// always use a NULL pStack for public access, 
	// almost always nDeep=1 to Inc() references of containment, unless custom refcounting.
	long IncRef(StackFrameCheck *pStack =0, int nDeep=1);
	
	// Decrements the reference count for this object.
	// If the count drops to zero, the object deletes itself.
	// nSubObjectsOnly=1 to DecRef() all contained objects but not 'this' object
	// nSubObjectsOnly=1 when 'this' is on the stack and should not be deleted on a refcount of 0
	long DecRef(int nSubObjectsOnly = 0, StackFrameCheck *pStack =0);
	// To DecRef() all contained objects, but not 'this'
	void DecRefContained(){ DecRef(1); m_nBehaviorFlags |=PREVENT_AUTO_DESTRUCTION;}
	long GetRefCount(){ return m_nRefCount; }

};

// runtime maps of active objects for extended object navigation.
class MapOfActiveObjects
{
	GHash m_hshActiveMaps;
public:
	MapOfActiveObjects(){}
	~MapOfActiveObjects(){}
	void AddActiveObjectMap(MemberDescriptor *pNew);
	void RemoveActiveObjectMap(MemberDescriptor *pOld);
	int IsActiveObjectMap(MemberDescriptor *pMap);
};
extern MapOfActiveObjects g_ActiveObjectMap;


#define GET_FACTORY(class_name) (class_name::FactoryCreate)

// Macro to create a typed instance of a deravitive
// object, and return via common base class type.
// DECLARE_FACTORY reserves xml_tag for future use
#define DECLARE_FACTORY(class_name, xml_tag)	public:	static const char *GetStaticTag();	static const char *GetStaticType();	static class_name *Attach(const char *pzOid);	static class_name *Attach(int nOid);	const char *GetVirtualType();	const char *GetVirtualTag();	static ObjectFactory GetStaticFactory(); static XMLObject* FactoryCreate();	void RegisterObject();
#define DECLARE_XML(class_name)                 public: static const char *GetStaticTag();	static const char *GetStaticType();	static class_name *Attach(const char *pzOid);	static class_name *Attach(int nOid);	const char *GetVirtualType();	const char *GetVirtualTag();	static ObjectFactory GetStaticFactory(); static XMLObject* FactoryCreate();	void RegisterObject();

class RegisterBusinessObject
{
public:
	RegisterBusinessObject(	ObjectFactory, 
							const char * szXMLTagIdentifier, 
							const char * szBusObjClassName);
};


// If you FTP the source as Binary, this MACRO won't compile under gcc.  Use ASCII.
#define IMPLEMENT_FACTORY_NO_REG(class_name, xml_tag)					\
const char * class_name::GetVirtualTag()								\
{																		\
	return class_name::GetStaticTag();									\
}																		\
class_name * class_name::Attach(int nOid)								\
{																		\
	char pzOid[20];	sprintf(pzOid,"%d",nOid);							\
	XMLObject *p = cacheManager.getObject(#xml_tag,pzOid,"",0);			\
	return (class_name *)p->GetUntypedThisPtr();						\
}																		\
class_name *class_name::Attach(const char *pzOid)						\
{																		\
	XMLObject *p = cacheManager.getObject(#xml_tag,pzOid,"",0);			\
	return (class_name *)p->GetUntypedThisPtr();						\
}																		\
const char * class_name::GetStaticTag()									\
{																		\
	static const char *pXMLTag = #xml_tag;								\
	return pXMLTag;														\
}																		\
ObjectFactory class_name::GetStaticFactory()							\
{																		\
	return class_name::FactoryCreate;									\
}																		\
const char * class_name::GetStaticType()								\
{																		\
	static const char *pClassName = #class_name;						\
	return pClassName;													\
}																		\
const char * class_name::GetVirtualType()								\
{																		\
	return class_name::GetStaticType();									\
}																		\
void class_name::RegisterObject()										\
{																		\
	SetObjectTypeName(#class_name);										\
	SetDefaultXMLTag(#xml_tag);											\
	m_pDerivedAddress = this;											\
	m_classFactory = class_name::FactoryCreate;							\
}																		\
XMLObject* class_name::FactoryCreate()									\
{																		\
	class_name *pTemp = new class_name();								\
	pTemp->RegisterObject();											\
	return (XMLObject*)pTemp;											\
}																		\

// ***********		IMPLEMENT_FACTORY	**************
#define IMPLEMENT_FACTORY(class_name, xml_tag)							\
IMPLEMENT_FACTORY_NO_REG(class_name, xml_tag)							\
RegisterBusinessObject class_name##Unique(								\
					class_name::FactoryCreate,							\
					#xml_tag,											\
					#class_name);										\

/*
#define IMPLEMENT_XML(class_name, xml_tag)	
IMPLEMENT_FACTORY_NO_REG(class_name, xml_tag)	
RegisterBusinessObject class_name##Unique(
				class_name::FactoryCreate, #xml_tag,#class_name);
*/



#define CORBA_COM_BASE(class_name, xml_tag)								\
const char * class_name::GetVirtualTag()								\
{																		\
	return class_name::GetStaticTag();									\
}																		\
const char * class_name::GetVirtualType()								\
{																		\
	return class_name::GetStaticType();									\
}																		\
																		\
const char * class_name::GetStaticTag(){								\
	static const char *pXMLTag = #xml_tag;								\
	return pXMLTag;														\
}																		\
ObjectFactory class_name::GetStaticFactory()							\
						{return class_name::FactoryCreate;}				\
const char * class_name::GetStaticType(){								\
	static const char *pClassName = #class_name;						\
	return pClassName;													\
}																		\
void class_name::RegisterObject()										\
{																		\
	SetObjectTypeName(#class_name);										\
	SetDefaultXMLTag(#xml_tag);											\
	m_pDerivedAddress = this;											\
	m_classFactory = class_name::FactoryCreate;							\
}																		\



// Bridge to Microsoft's COM ATL (Active Template Library)
#define IMPLEMENT_ATL_FACTORY(class_name, xml_tag)						\
CORBA_COM_BASE(class_name, xml_tag)										\
																		\
XMLObject* class_name::FactoryCreate()									\
{																		\
	CComObject<class_name>* pTemp = NULL;								\
	CComObject<class_name>::CreateInstance(&pTemp);						\
	pTemp->RegisterObject();											\
	return (XMLObject*)pTemp;											\
}																		\
																		\
RegisterBusinessObject class_name##Unique(								\
				class_name::FactoryCreate,								\
				#xml_tag,												\
				#class_name);											\


// Bridge to OMG's ORB (Object Request Broker)
#define IMPLEMENT_ORB_FACTORY(class_name, xml_tag)						\
CORBA_COM_BASE(class_name, xml_tag)										\
																		\
XMLObject* class_name::FactoryCreate()									\
{																		\
	class_name *pImpl = new class_name();								\
	pImpl->RegisterObject();											\
	return (XMLObject *)pImpl;											\
}																		\
																		\
RegisterBusinessObject class_name##Unique(								\
				class_name::FactoryCreate,								\
				#xml_tag,												\
				#class_name);											\


#endif //_XML_BASE_OBJECT_SUPPORT__
