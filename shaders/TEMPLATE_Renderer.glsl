#include <FAKE_INCLUDE/glsl_fake.hxx>
//$END_FAKE$

//-- Vertex
#version 150

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

//-- Fragment
#version 150

in vec2 TexCoord;

uniform float AAScale;
uniform int AALevel;
uniform float InvAALevel;

uniform float Time;

uniform float XMin = -1.0f;
uniform float YMax = 1.0f;
uniform float ImageVirtualWidth;
uniform float ImageVirtualHeight;


//<VARIABLES>
uniform sampler2D Tex1;

out vec4 FragColor;
vec3 getPixelColor(vec2 coords) {

    float radius = sqrt(coords.x * coords.x + coords.y * coords.y);
    float angle = atan(coords.x, coords.y);

    //<EQUATIONS>
    
    return texture(Tex1, vec2(u, v)).xyz;
}

void main() {
//    float imgX = TexCoord.x * 2.0f - 1.0f;
    //float imgY = TexCoord.y * 2.0f - 1.0f;
    float imgX = XMin + TexCoord.x * ImageVirtualWidth;
    float imgY = YMax - TexCoord.y * ImageVirtualHeight;

    vec3 color = vec3(0.0, 0.0, 0.0);

    if (AALevel == 1) {
        FragColor = vec4(getPixelColor(vec2(imgX, imgY)), 1.0f);
    } else {
        for (int x = 0; x < AALevel; x++) {
            for (int y = 0; y < AALevel; y++) {

                color += getPixelColor(vec2(imgX, imgY)
                        + vec2(x, y) * InvAALevel * AAScale);
            }
        }

        FragColor = vec4(color * InvAALevel * InvAALevel, 1.0f);
    }
}

